<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Adminhtml_Menumanager_MenuController extends Mage_Adminhtml_Controller_Action{
    
    protected function _construct()
    {
        parent::_construct();
        $this->setUsedModuleName('Magemonks_Menumanager');
        $this->_helper = Mage::helper('menumanager');
    }     

    /**
     * Init layout, menu and breadcrumb
     *
     * @return Magemonks_Menu_Adminhtml_MenuController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/menumanager')
            ->_addBreadcrumb(Mage::helper('menumanager')->__('CMS'), Mage::helper('menumanager')->__('CMS'))
            ->_addBreadcrumb(Mage::helper('menumanager')->__('Menumanager'), Mage::helper('menumanager')->__('Menumanager'))
        ;
        return $this;
    }    
    
    /**
     * Index action
     */    
    public function indexAction()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Menumanager'));
        $this->_initAction();
        $this->renderLayout();
    }
    
    /**
     * Create action
     */
    public function newAction()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Menumanager'))->_title($this->__('New Menu'));

        $model = Mage::getModel('menumanager/menu');
        Mage::register('menumanager_menu', $model);
        
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }        

        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('menumanager')->__('New Menu'), Mage::helper('menumanager')->__('New Menu'))
            ->renderLayout();
    }

    /**
     * Edit action
     */
    public function editAction()
    {
        $this->_title($this->__('CMS'))->_title($this->__('Menumanager'));

        $id = $this->getRequest()->getParam('id');

        if (!$id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('menumanager')->__('This menu no longer exists.'));
                $this->_redirect('*/*/');
                return;
        }
        
        $menu = Mage::getModel('menumanager/menu')->load($id);
        if (! $menu->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('menumanager')->__('This menu no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        $this->_title($menu->getData('label'));

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $menu->setData($data);
        }

        Mage::register('menumanager_menu', $menu);

        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('menumanager')->__('Edit Menu'), Mage::helper('menumanager')->__('Edit Menu'))
            ->renderLayout();
        
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('menumanager/menu')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('menumanager')->__('This menu no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            // init model and set data
            $model->setData($data);

            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('menumanager')->__('The menu has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('id')) {
            $label = "";
            try {
                // init model and delete
                $model = Mage::getModel('menumanager/menu');
                $model->load($id);
                $label = $model->getData('label');
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('menumanager')->__('The menu has been deleted.'));
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('menumanager')->__('Unable to find a menu to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/menumanager');
    }     
    
}