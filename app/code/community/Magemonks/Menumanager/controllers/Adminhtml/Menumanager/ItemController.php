<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Adminhtml_Menumanager_ItemController extends Mage_Adminhtml_Controller_Action{
    
    protected function _construct()
    {
        parent::_construct();
        $this->setUsedModuleName('Magemonks_Menumanager');
        $this->_menumanagerhelper = Mage::helper('menumanager');
        $this->_magemonkshelper = Mage::helper('magemonks');

    }     

    /**
     * Init layout, menu and breadcrumb
     *
     * @return Magemonks_Menu_Adminhtml_MenuController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('cms/menumanager')
            ->_addBreadcrumb($this->_menumanagerhelper->__('CMS'), $this->_menumanagerhelper->__('CMS'))
            ->_addBreadcrumb($this->_menumanagerhelper->__('Menumanager'), $this->_menumanagerhelper->__('Menumanager'))
            ->_addBreadcrumb($this->_menumanagerhelper->__('Item'), $this->_menumanagerhelper->__('Item'))
        ;
        return $this;
    }
    
    
    /**
     * Show AJAX Response
     *
     * @param array $data
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    protected function _showAjaxResponse($data = array())
    {
        if(!isset($data['success'])){
            if(isset($data['error'])){
                $data['success'] = false;
            }
            else{
                $data['success'] = true;
            }
        }
        
        $json = $this->_magemonkshelper->getJsonWrapper($data);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));        
        return $this;
    }    
    
    
    /**
     * Index action
     */
    public function indexAction()
    {
        return $this->_forward('edit');
    }
    
    
    /**
     * Edit action
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function editAction()
    {
        $ajax = (bool) $this->getRequest()->getParam('isAjax', false);
        
        $this->_title($this->__('CMS'))->_title($this->__('Menumanager'));
        
        $id = $this->getRequest()->getParam('id');

        // Load the item
        if(!$id ){
            if($ajax){
                return $this->_showAjaxResponse(array('error' => true));
            }
            else{
                Mage::getSingleton('adminhtml/session')->addError($this->_menumanagerhelper->__('Item ID was not set'));
                $this->_redirect('*/menumanager_menu/');
                return;  
            }            
        }
                
        $item = Mage::getModel('menumanager/item')->load($id);
        if (!$item->getId()) {
            if($ajax){
                return $this->_showAjaxResponse(array('error' => true));
            }
            else{
                Mage::getSingleton('adminhtml/session')->addError($this->_menumanagerhelper->__('This item no longer exists.'));
                $this->_redirect('*/menumanager_menu/');
                return;
            }
        }

        $menu = $item->getMenu();

        Mage::register('menumanager_menu', $menu);
        Mage::register('menumanager_item', $item);

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $item->setData($data);
        }

        $this->_initAction()
            ->_addBreadcrumb($this->_menumanagerhelper->__('Edit Menu'), $this->_menumanagerhelper->__('Edit Menu'))
            ->_addBreadcrumb($this->_menumanagerhelper->__('Edit Item'), $this->_menumanagerhelper->__('Edit Item'));
        
        if($ajax){
            $data = array(
                'content' => $this->getLayout()->getBlock('item.edit')->getFormHtml(),
                'messages' => $this->getLayout()->getMessagesBlock()->getGroupedHtml()
            );
            return $this->_showAjaxResponse($data);
            
        }
        
        $this->_title($menu->getData('label'))->_title($item->getData('label'));
        $this->renderLayout();
    }

    /**
     * Save action
     *
     * @return Magemonks_Menumanager_Adminhtml_ItemController
     */
    public function saveAction()
    {
        $ajax = (bool) $this->getRequest()->getParam('isAjax', false);
        
        if ($data = $this->getRequest()->getPost()) {
            $id = $this->getRequest()->getParam('id');

            // Load the item
            if(!$id){
                if($ajax){
                    return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_menumanagerhelper->__('This item no longer exists.')));
                }
                else{
                    Mage::getSingleton('adminhtml/session')->addError($this->_menumanagerhelper->__('This item no longer exists.'));
                    $this->_redirect('*/menumanager_menu/');
                    return;  
                }            
            }

            $item = Mage::getModel('menumanager/item')->load($id);
            if (!$item->getId()) {
                if($ajax){
                    return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_menumanagerhelper->__('This item no longer exists.')));
                }
                else{
                    Mage::getSingleton('adminhtml/session')->addError($this->_menumanagerhelper->__('This item no longer exists.'));
                    $this->_redirect('*/menumanager_menu/');
                    return;
                }
            }

            //fix for empty multiselect elements
            $form = $item->getAdminForm();
            $fields = $item->getConfigurationFields();
            foreach($fields as $field){
                $element = $form->getElement($field);
                if(!is_null($element) && $element instanceof Varien_Data_Form_Element_Multiselect){
                    if(!isset($data[$field]) || !is_array($data[$field])){
                        $data[$field] = array();
                    }
                }
            }

            //copy so the original can be used later
            $itemdata = $data;

            $enable = false;
            if(isset($itemdata['form_key'])) unset($itemdata['form_key']);
            if(isset($itemdata['isAjax'])) unset($itemdata['isAjax']);
            if(isset($itemdata['isEnable'])){
                unset($itemdata['isEnable']);
                $enable = true;
            }

            $item->set($itemdata);
            $enabled = false;

            try {
                $item->save();

                if($enable){
                    if($item->canDisplay()){
                        $item->setData('is_active', 1)->save();
                        $enabled = true;
                    }
                }

                if($ajax){
                    $this->getLayout()->getMessagesBlock()->addMessage(Mage::getSingleton('core/message')->success($this->_menumanagerhelper->__('The item has been saved.')));
                    if($enable && $enabled){
                        $this->getLayout()->getMessagesBlock()->addMessage(Mage::getSingleton('core/message')->success($this->_menumanagerhelper->__('The item has been enabled.')));
                    }
                    elseif($enable && !$enabled){
                        $this->getLayout()->getMessagesBlock()->addMessage(Mage::getSingleton('core/message')->error(Mage::helper('menumanager')->__('The item could not be enabled.')));
                    }

                    $response = array(
                        'messages' => $this->getLayout()->getMessagesBlock()->getGroupedHtml(),
                        'id' => $item->getId(),
                        'new_column_width' => $item->get('width'),
                    );
                    if($enabled){
                        $response['new_status'] = $item->get('is_active');
                    }

                    return $this->_showAjaxResponse($response);
                }
                else{
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('menumanager')->__('The item has been saved.'));
                    if($enable && $enabled){
                        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('menumanager')->__('The item has been enabled.'));
                    }
                    elseif($enable && !$enabled){
                        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('menumanager')->__('The item could not be enabled.'));

                    }
                    Mage::getSingleton('adminhtml/session')->setFormData(false);

                    $this->_redirect('*/*/edit', array('id' => $item->getId()));
                }

                return;

            } catch (Exception $e) {
                if($ajax){
                    return $this->_showAjaxResponse(array('error' => true, 'message' => $e->getMessage()));
                }
                else{
                    // display error message
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    // save data in session
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    // redirect to edit form
                    $this->_redirect('*/*/edit', array('id' => $item->getId()));
                }                

                return;
            }            
        }
    }
    
    /**
     * Ajax action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController|void
     */
    public function ajaxAction()
    {
        switch($this->getRequest()->getParam('operation'))
        {
            case 'create':
                return $this->_forward('ajaxcreate');
                break;
            case 'delete':
               return  $this->_forward('ajaxdelete');
                break;
            case 'move':
                return $this->_forward('ajaxmove');
                break;
            case 'rename':
                return $this->_forward('ajaxrename');
                break;
            case 'enable':
                return $this->_forward('ajaxenable');
                break;
            case 'disable':
                return $this->_forward('ajaxdisable');
                break;            
            case 'getcontextmenu':
                return $this->_forward('ajaxgetcontextmenu');
                break;             
            default:
                return $this->_showAjaxResponse(array('error' => true));
                break;
        }
        return $this;
    }
    
    
    /**
     * Get context menu action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function ajaxgetcontextmenuAction()
    {
        $request = $this->getRequest();
        
        $id = $request->getParam('id', null);
        
        if(empty($id)){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        //get item
        $item = Mage::getModel('menumanager/item')->load($id);

        //item has to exist
        if(!$item->getId()){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        $data = $item->getAdminContextMenu();
        
        $this->_showAjaxResponse(array('data' => $data));
        
    }   
    
    /**
     * Enable action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function ajaxenableAction()
    {
        $request = $this->getRequest();
        
        $id = $request->getParam('id', null);
        
        if(empty($id)){
            return $this->_showAjaxResponse(array('error' => true, 'message' => Mage::helper('menumanager')->__('The id was empty.')));
        }
        
        //get item
        $item = Mage::getModel('menumanager/item')->load($id);
        //item has to exist
        if(!$item->getId()){
            return $this->_showAjaxResponse(array('error' => true, 'message' => Mage::helper('menumanager')->__('The item does not exist.')));
        }

        $enabled = false;
        try{
            if($item->canDisplay()){
                $item->setData('is_active', 1)->save();
                $enabled = true;
            }
        }
        catch(Exception $e){
            return $this->_showAjaxResponse(array('error' => true, 'message' => $e->getMessage()));
        }
        
        if($item->getData('is_active') != 1){
            return $this->_showAjaxResponse(array('error' => true));
        }

        if($enabled){
            return $this->_showAjaxResponse(array('id' => $id));
        }
        else{
            return $this->_showAjaxResponse(array('error' => true, 'message' => Mage::helper('menumanager')->__('The item could not be enabled.')));
        }

    }    
    
    /**
     * Disable action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function ajaxdisableAction()
    {
        $request = $this->getRequest();
        
        $id = $request->getParam('id', null);
        
        if(empty($id)){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        //get item
        $item = Mage::getModel('menumanager/item')->load($id);
        //item has to exist
        if(!$item->getId()){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        try{
            $item->setData('is_active', 0)->save();
        }
        catch(Exception $e){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        if($item->getData('is_active') != 0){
            return $this->_showAjaxResponse(array('error' => true));
        }

        return $this->_showAjaxResponse(array('id' => $id));
    }      
    
    /**
     * Rename action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function ajaxrenameAction()
    {
        $request = $this->getRequest();
        $id = $request->getParam('id', null);
        $label = $request->getParam('label', null);
        
        if(empty($id) || empty($label)){
            return $this->_showAjaxResponse(array('error' => true, 'message' => Mage::helper('menumanager')->__('The id was empty.')));
        }
        
        //get item
        $item = Mage::getModel('menumanager/item')->load($id);
        //item has to exist
        if(!$item->getId()){
            return $this->_showAjaxResponse(array('error' => true, 'message' => Mage::helper('menumanager')->__('The item does not exist.')));
        }
        
        try{
            $item->setData('label', $label)->save();
        }
        catch(Exception $e){
            return $this->_showAjaxResponse(array('error' => true, 'message' => $e->getMessage()));
        }
        
        return $this->_showAjaxResponse(array('id' => $id));
    }
    
    
    /**
     * Move action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function ajaxmoveAction()
    {
        $request = $this->getRequest();
        
        //print_r($request->getParams());die;
        
        $parent_id = $request->getParam('parent_id', null);
        $id = $request->getParam('id', null);
        $position = $request->getParam('position', null);
        
        if(empty($parent_id) || empty($id) || !is_numeric($position)){
            return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_menumanagerhelper->__('Not all pramaters were set')));
        }        
        
        //get item
        $item = Mage::getModel('menumanager/item')->load($id);
        //item has to exist
        if(!$item->getId()){
            return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_menumanagerhelper->__('The item does noet exist (anymore)')));
        }
        
        //get parent
        $parent = Mage::getModel('menumanager/item')->load($parent_id);
        //parent has to exist
        if(!$parent->getId()){
            return $this->_showAjaxResponse(array('error' => true, 'message' => $this->_menumanagerhelper->__('The parent does noet exist (anymore)')));
        }
        
        try{
            $parent->addChild($item, $position);
        }
        catch(Exception $e){
            return $this->_showAjaxResponse(array('error' => true, 'message' => $e->getMessage()));
        }        
        
        return $this->_showAjaxResponse();
        
    }    
    
    /**
     * Create action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function ajaxcreateAction()
    {
        $request = $this->getRequest();
        
        $parent_id = $request->getParam('parent_id', null);
        $type = $request->getParam('type', null);
        $label = $request->getParam('label', null);
        
        //parent_id, type and label can not be empty
        if(empty($parent_id) || empty($type) || empty($label)){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        //get parent
        $parent = Mage::getModel('menumanager/item')->load($parent_id);
        //parent has to exist
        if(!$parent->getId()){
            return $this->_showAjaxResponse(array('error' => true));
        }        
        
        $type = strtolower($type);
        $modelpath = 'menumanager/item_'.$type;
        try{
            $item = Mage::getModel($modelpath);
        }
        catch(Exception $e){
            return $this->_showAjaxResponse(array('error' => true, 'message' => $e->getMessage()));
        }
      
        $item->setData('label', $label);
        $parent->addChild($item);
        
        if($item->getData('id')){
            return $this->_showAjaxResponse(array(
                'id' => $item->getData('id'),
                "edit_url" => Mage::getModel('adminhtml/url')->getUrl('*/*/edit', array('id' => $item->getData('id')))
            ));
        }
        else{
            return $this->_showAjaxResponse(array('error' => true));
        }
    }
    
    /**
     * Delete action
     *
     * @return Magemonks_Menumanager_Adminhtml_Menumanager_ItemController
     */
    public function ajaxdeleteAction()
    {
        $request = $this->getRequest();
        
        $id = $request->getParam('id', null);
        
        if(empty($id)){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        //get item
        $item = Mage::getModel('menumanager/item')->load($id);
        //item has to exist
        if(!$item->getId()){
            return $this->_showAjaxResponse(array('error' => true));
        }
        if($item->getData('type') === 'root'){
            return $this->_showAjaxResponse(array('error' => true));
        }
        
        $item->delete();
        return $this->_showAjaxResponse();
    }

    /**
     *  Is it allowed to access this controller?
     *
     * @return Boolean isAllowed
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/menumanager');
    }
}