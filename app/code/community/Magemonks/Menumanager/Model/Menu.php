<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Menu extends Mage_Core_Model_Abstract
{
    const CACHE_TAG              = 'menumanager';
    protected $_cacheTag         = 'menumanager';

    public $rootItem = null;
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('menumanager/menu');
    }

    /**
     * Fetch the menu's items
     * 
     * @param Boolean $includeRoot
     * @return Magemonks_Menumanager_Model_Resource_Item_Collection
     */    
    public function getItems($includeRoot = false)
    {
        return $this->getResource()->getItems($this, $includeRoot);
    }
    
    
    
    /**
     * Get the root item from the tree
     * 
     * @return type Magemonks_Menumanager_Model_Item
     */
    public function getRootItem()
    {
        if(!is_null($this->rootItem)){
            return $this->rootItem;
        }

        $items = Mage::getModel('menumanager/item')->getCollection()
                ->addFieldToFilter('menu_id', $this->getId())
                ->addFieldToFilter('level', 0);
        if(count($items) !== 1){
            Mage::throwException(Mage::helper('menumanager')->__('No root node could be found for menu id %s', $this->getId()));
        }


        $this->rootItem = $items->getFirstItem();

        return $this->rootItem;
    }


    /**
     * @param $id
     * @param Magemonks_Menumanager_Model_Item|null $parent
     * @return null
     */
    public function getItemById($id, Magemonks_Menumanager_Model_Item $parent = null)
    {
        if($parent){
            $items = $parent->getChildren();
        }
        else{
            $items = $this->getItems(true);
        }

        foreach($items as $item){
            if($item->get('id') == $id){
                return $item;
            }
            if(count($item->getChildren()) > 0){
                $item = $this->getItemById($id, $item);
                if(!is_null($item)){
                    return $item;
                }
            }
        }
        return null;
    }

    /**
     * Add an item to the menu
     *
     * @param Magemonks_Menumanager_Model_Item $item
     * @param null|Magemonks_Menumanager_Model_Item $parent
     * @param $position
     * @return Magemonks_Menumanager_Model_Item
     */
    public function addItem(Magemonks_Menumanager_Model_Item $item, Magemonks_Menumanager_Model_Item $parent, $position = null)
    {
        if(!is_a($parent, 'Magemonks_Menumanager_Model_Item') || !$parent->getId()){
            Mage::throwException(Mage::helper('menumanager')->__('Parent node could not be found'));
        }
        $parent->addChild($item, $position);

        return $item;
    }

    /**
     * Get the allowed item types
     *
     * @return array
     */
    public function getAllowedItemTypes()
    {
        $modelPaths = Mage::helper('menumanager/loader')->getItemTypes();
        if(!$this->getId()) return $modelPaths;

        $disallowedModelPaths = $this->getData('disallowed_item_types');
        $return = array();

        foreach($modelPaths as $modelPath){
            if(!in_array($modelPath, $disallowedModelPaths)) $return[] = $modelPath;
        }

        return $return;
    }

}