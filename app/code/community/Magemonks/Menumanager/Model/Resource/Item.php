<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Resource_Item extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {    
        $this->_init('menumanager/item', 'id');
    }


    /**
     * Perform oparation after loading the object
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Menumanager_Model_Resource_Item
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        //convert dates to Local timezone
        $this->_convertDate($object, 'active_from', false)->_convertDate($object, 'active_to', false);

        return $this;
    }

    /**
     * Convert and set the correct date
     *
     * @param Mage_Core_Model_Abstract $object
     * @param $attribute
     * @param bool $toDb
     * @return Magemonks_Menumanager_Model_Resource_Item
     */
    protected function _convertDate(Mage_Core_Model_Abstract $object, $attribute, $toDb = true)
    {
        $date = $object->getData($attribute);
        if($date){
            if($toDb){
                $date = Mage::getSingleton('core/date')->gmtDate(null, $date);
                if(empty($date)){
                    Mage::throwException(Mage::helper('menumanager')->__('Invalid date input'));
                }
            }
            else{
                $date = Mage::getSingleton('core/date')->date(null, $date);
                if(empty($date)){
                   $date = null;
                }
            }

            $object->setData($attribute, $date);
        }
        return $this;
    }

    /**
     * @param $object
     * @param $attribute
     * @return Magemonks_Menumanager_Model_Resource_Item
     */
    protected function _addCronJob($object, $attribute)
    {
        $date = $object->getData($attribute);
        if($date){
            //remove existing
            $crons = Mage::getModel('cron/schedule')->getCollection()
                ->addFieldToFilter('job_code', 'menumanager_item_schedule_check')
                ->addFieldToFilter('scheduled_at', $date);

            foreach($crons as $cron){
                $cron->delete();
            }

            //add new
            $cron = Mage::getModel('cron/schedule')->setData(array(
                'job_code' => 'menumanager_item_schedule_check',
                'scheduled_at'=> $date,
                'created_at' => Mage::getSingleton('core/date')->gmtDate(),
                'status' => Mage_Cron_Model_Schedule::STATUS_PENDING
            ));
            $cron->save();
        }
        return $this;
    }

    /**
     * Perform operations before object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Menumanager_Model_Resource_Item
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        //is operation allowed? (skip for unsaved root items)
        if($object->getData('type') != 'root' && !is_numeric($object->getData('parent_id'))){
            Mage::throwException('This operation is not allowed (No parent id)');
        }
        if($object->getData('type') != 'root' && $object->getParent()){
            $parent = $object->getParent();
            if(!$parent->canBeAParentOf($object)){
                Mage::throwException('This operation is not allowed (canBeAParentOf)');
            }
            if(!$object->canBeAChildOf($parent)){
                Mage::throwException('This operation is not allowed (canBeAChildOf)');
            }
        }
        if($object->getData('type') != 'root' && !$object->canSave()){
            Mage::throwException('This operation is not allowed (canSave)');
        }



        //Set creation and update time
        if (! $object->getId()) {
            $object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
        }
        $object->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());

        //convert dates to GMT
        $this->_convertDate($object, 'active_from')->_convertDate($object, 'active_to');
        $this->_addCronJob($object, 'active_from')->_addCronJob($object, 'active_to');

        //operations to maintain tree structure
        if(!$object->getId()){
            //Item is new
            $connection  = $this->_getWriteAdapter();
            $connection->beginTransaction();
            $connection->query("update ".$this->getMainTable()." set position = position+1 where parent_id = ? and position >= ?", array($object->getData('parent_id'), $object->getData('position')));
            $connection->commit();
        }
        else{
            $old = $object->getOrigData();
            $new = $object->getData();
            
            if($old['parent_id'] != $new['parent_id']){
                //Item has new parent
                $connection  = $this->_getWriteAdapter();

                $connection->beginTransaction();
                
                //1: Update position of items under old parent;
                $connection->query("update ".$this->getMainTable()." set position = position-1 where parent_id = ? and position > ?", array($old['parent_id'], $old['position']));
                
                //2: Update position of items under new parent;
                $connection->query("update ".$this->getMainTable()." set position = position+1 where parent_id = ? and position >= ?", array($new['parent_id'], $new['position']));
                
                $connection->commit();                
            }
            elseif($old['position'] != $new['position']){
                //Item has new position
                $connection  = $this->_getWriteAdapter();

                //1: Update position of items under parent;
                $connection->beginTransaction();
                //set position to the items as if the new item was not there
                $connection->query("update ".$this->getMainTable()." set position = position-1 where parent_id = ? and position >= ?", array($old['parent_id'], $old['position']));
                
                //add 1 to the position of all items having a heigher position than item's position
                $connection->query("update ".$this->getMainTable()." set position = position+1 where parent_id = ? and position >= ?", array($old['parent_id'], $new['position']));
                $connection->commit();
            }
        }
        
        return $this;
    }

    /**
     * Perform operations after delete
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Menumanager_Model_Resource_Item
     */
    public function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        //operations to maintain tree structure
        if($object->getId()){
            $connection  = $this->_getWriteAdapter();
            $connection->beginTransaction();
            $connection->query("update ".$this->getMainTable()." set position = position-1 where parent_id = ? and position >= ?", array($object->getData('parent_id'), $object->getData('position')));
            $connection->commit();

        }
        return $this;
    }
}