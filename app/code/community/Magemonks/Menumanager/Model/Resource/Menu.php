<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Resource_Menu extends Mage_Core_Model_Resource_Db_Abstract
{
    
    
    /**
     * Should the root item be automatticalliy created upon save
     * 
     * @var type Boolean
     */
    protected $_createRootItem = false;
    
    
    public function _construct()
    {    
        $this->_init('menumanager/menu', 'id');
    }
    
    
   /**
     * Process menu data before deleting
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Page
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $condition = array(
            'menu_id = ?'     => (int) $object->getId(),
        );

        $this->_getWriteAdapter()->delete($this->getTable('menumanager/menu_store'), $condition);

        return parent::_beforeDelete($object);
    }

    /**
     * Perform operations before object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Menumanager_Model_Resource_Menu
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $disallowedItemTypes = $object->getData('disallowed_item_types', null);
        if(!is_array($disallowedItemTypes)) $disallowedItemTypes = array();
        $object->setData('disallowed_item_types', serialize($disallowedItemTypes));


        if (!$this->getIsUniqueMenuToStores($object)) {
            Mage::throwException(Mage::helper('menumanager')->__('A menu identifier with the same properties already exists in the selected store.'));
        }

        if (! $object->getId()) {
            $object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
            $this->_createRootItem = true;
        }
        $object->setData('update_time', Mage::getSingleton('core/date')->gmtDate());
        return $this;
    }

    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Menumanager_Model_Resource_Menu
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $object->setData('disallowed_item_types', unserialize($object->getData('disallowed_item_types')));

            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);

            $this->_prepareItemsStructrue($object);

        }
        return parent::_afterLoad($object);
    }

    /**
     * Perform operations after object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Menumanager_Model_Resource_Menu
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        
        /* Set the stores */
        
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();

        $table  = $this->getTable('menumanager/menu_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);

        if ($delete) {
            $where = array(
                'menu_id = ?'     => (int) $object->getId(),
                'store_id IN (?)' => $delete
            );

            $this->_getWriteAdapter()->delete($table, $where);
        }

        if ($insert) {
            $data = array();

            foreach ($insert as $storeId) {
                $data[] = array(
                    'menu_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }
        
        
        
        /* Create the root item */
        if($this->_createRootItem === true){
            $item = Mage::getModel('menumanager/item_root');
            $item->setData('menu_id', $object->getId());
            $item->save();
        }

        $this->_prepareItemsStructrue($object);

        return parent::_afterSave($object);

    }

    /**
     * Load an object using 'identifier' field if there's no field specified and value is not numeric
     *
     * @param Mage_Core_Model_Abstract $object
     * @param mixed $value
     * @param string $field
     * @return Magemonks_Menumanager_Model_Resource_Menu
     */
    public function load(Mage_Core_Model_Abstract $object, $value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'identifier';
        }

        return parent::load($object, $value, $field);
    }


    /**
     * Prepare the item sturcture so all items can be loaded at once in the right tree structure
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Magemonks_Menumanager_Model_Resource_Menu
     */
    protected function _prepareItemsStructrue(Mage_Core_Model_Abstract $object)
    {
        $items = $this->getItems($object, true);
        $itemsArray = array();
        
        foreach($items as $item){
            $item->menu = $object;
            if($item->getData('type') === 'root'){
                $itemsArray[$item->getId()] = $item;
                $object->rootItem = $item;
            }
            elseif(array_key_exists($item->getParentId(), $itemsArray)){
                $parent = $itemsArray[$item->getParentId()];
                $parent->addChild($item, null, true);
            }
            $item->children = array();

            $itemsArray[$item->getId()] = $item;
        }

        $request = Mage::app()->getRequest();
        if($request && $request->getModuleName() != 'admin'){
            $a = array('bWFnZW1vbmtzL2xpY2Vuc2U=','aXNTZXJpYWxWYWxpZA==', 'Z2V0U2VyaWFs', 'Z2V0UHJvZHVjdENvZGU=', 'TWFnZU1vbmtz', 'aHR0cDovL3d3dy5tYWdlbW9ua3MuY29t', 'bWVudW1hbmFnZXIvaXRlbV9jdXN0b21saW5r');
            if(!Mage::helper(base64_decode($a[0]))->{(base64_decode($a[1]))}(Mage::helper('menumanager')->{(base64_decode($a[2]))}(), Mage::helper('menumanager')->{(base64_decode($a[3]))}())){
                $item = Mage::getModel(base64_decode($a[6]))->set('title', base64_decode($a[4]))->set('url', base64_decode($a[5]))->set('is_active', 1);$item->menu = $object; $object->rootItem->addChild($item, null, true);
            }
        }

        return $this;
    }
    
    
    /**
     * Fetch the menu's items
     * 
     * @param Mage_Core_Model_Abstract $object
     * @param Boolean $includeRoot
     * @return Magemonks_Menumanager_Model_Resource_Item_Collection
     */
    public function getItems(Mage_Core_Model_Abstract $object, $includeRoot = false)
    {
        $items = Mage::getModel('menumanager/item')->getCollection()
                ->addFieldToFilter('menu_id', $object->getId());
        
        if($includeRoot !== true){
            $items->addFieldToFilter('level', array('neq' => 0));
        }
        
        $items->addOrder('level', Varien_Data_Collection::SORT_ORDER_ASC)->addOrder('position', Varien_Data_Collection::SORT_ORDER_ASC);
        
        return $items;
    }    
    

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param Magemonks_Menumanager_Model_Menu $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {
            $stores = array(
                (int) $object->getStoreId(),
                Mage_Core_Model_App::ADMIN_STORE_ID,
            );

            $select->join(
                array('mms' => $this->getTable('menumanager/menu_store')),
                $this->getMainTable().'.id = mms.menu_id',
                array('store_id')
            )->where('is_active = ?', 1)
            ->where('mms.store_id in (?) ', $stores)
            ->order('store_id DESC')
            ->limit(1);
        }

        return $select;
    }

    /**
     * Check for unique of identifier of menu to selected store(s).
     *
     * @param Mage_Core_Model_Abstract $object
     * @return bool
     */
    public function getIsUniqueMenuToStores(Mage_Core_Model_Abstract $object)
    {
        if (Mage::app()->isSingleStoreMode()) {
            $stores = array(Mage_Core_Model_App::ADMIN_STORE_ID);
        } else {
            $stores = (array)$object->getData('stores');
        }

        $select = $this->_getReadAdapter()->select()
            ->from(array('mm' => $this->getMainTable()))
            ->join(
                array('mms' => $this->getTable('menumanager/menu_store')),
                'mm.id = mms.menu_id',
                array()
            )->where('mm.identifier = ?', $object->getData('identifier'))
            ->where('mms.store_id IN (?)', $stores);

        if ($object->getId()) {
            $select->where('mm.id <> ?', $object->getId());
        }

        if ($this->_getReadAdapter()->fetchRow($select)) {
            return false;
        }

        return true;
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('menumanager/menu_store'), 'store_id')
            ->where('menu_id = :menu_id');

        $binds = array(
            ':menu_id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }    
}