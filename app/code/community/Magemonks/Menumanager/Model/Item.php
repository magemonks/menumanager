<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item extends Magemonks_Core_Model_Configurationfields_Abstract
{

    const CACHE_TAG              = 'menumanager';
    protected $_cacheTag         = 'menumanager';


    /**
     * Config fields saved (serialized) in de DB
     *
     * @var array
     */
    public $configFields = array();

    /**
     * Children
     *
     * @var null|array
     */
    public $children = null;

    /**
     *  Defines the types this item can be a child of
     *
     * @var array
     */
    public $allowedParentTypes = array('all');

    /**
     *  Defines the types this item can be a parent of
     *
     * @var array
     */
    public $allowedChildTypes = array('all');

    /**
     *  Defines the types this item can be a child of
     *
     * @var array
     */
    public $disAllowedParentTypes = array();

    /**
     * @var null|Magemonks_Menumanager_Model_Menu
     */
    public $menu = null;

    /**
     * @var int
     */
    public $maxDepth = -1;

    /**
     * @var int
     */
    public $minDepth = -1;

    /**
     * @var int
     */
    public $maxChildren = -1;

    /**
     * @var int
     */
    public $maxSiblings = -1;

    /**
     * Default block
     *
     * @var string
     */
    public $blockPath = "menumanager/item";

    /**
     * Default template
     *
     * @var string
     */
    public $template = "magemonks/menumanager/item.phtml";

    /**
     * Label shown in tree when creating the item
     * @var
     */
    public $creationLabel;

    /**
     * Default iconfile
     *
     * @var string
     */
    public $iconFile = 'default.png';


    /**
     * Connection
     *
     * Possible values: menu / megamenu / menu-megamenu / megamenu-menu
     *
     * Type             Wrapp children as submenu      Allowed parents              Examples
     * ---------------------------------------------------------------------------------------------
     * menu             yes                            menu / megamenu-menu         label / customroute / category / product / customlink / multicategory
     * megamenu         no                             megamenu / menu-megamenu     cmsblock / image / wysiwyg / column / row / header / divider / spacer
     * menu-megamenu    no                             menu                         megamenu
     * megamenu-menu    yes                            megamenu                     megamenu_menu
     *
     * @var string
     */
    public $connection = "menu";


    /**
     * Constructor
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('menumanager/item');
    }

    public function getConfigurationFields(){
        return array_merge(array('display_mode', 'title', 'sub_title', 'title_color', 'image_url', 'image_alt', 'image_width', 'image_height', 'customers_group_ids', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'anchor_tag_id', 'item_tag_class', 'item_tag_id', 'submenu_tag_class', 'submenu_tag_id', 'custom_template', 'custom_block', 'html_before', 'html_after'), $this->configFields);
    }

    /**
     * Get's the items child items in ascending order
     *
     * @return  Array
     */
    final public function getChildren()
    {
        if(is_null($this->children)){
            $this->children = Mage::getModel('menumanager/item')->getCollection()
                ->addFieldToFilter('menu_id', $this->getMenuId())
                ->addFieldToFilter('level', $this->getLevel() + 1)
                ->addFieldToFilter('parent_id', $this->getId())
                ->addOrder('position', Varien_Data_Collection::SORT_ORDER_ASC)
                ->toSimpleArray();
        }
        return $this->children;
    }

    /**
     * Get the menu
     *
     * @param bool $returnEmpty
     * @return false|Mage_Core_Model_Abstract|null|Magemonks_Menumanager_Model_Menu
     */
    public function getMenu($returnEmpty = false)
    {
        if(is_null($this->menu)){
            if($this->getData('menu_id')){
                $this->menu = Mage::getModel('menumanager/menu')->load($this->getData('menu_id'));
            }
            elseif($returnEmpty === true){
                return Mage::getModel('menumanager/menu');
            }
            else{
                return null;
            }
        }
        return $this->menu;
    }

    /**
     * Get the items parent
     *
     * @return Magemonks_Menumanager_Model_Item
     */
    public function getParent()
    {
        if(is_null($this->parent)){
            $this->parent = Mage::getModel('menumanager/item')->load($this->getData('parent_id'));
        }
        return $this->parent;
    }

    /**
     * Find the closes parent of a given type
     *
     * @param $type
     * @return null|Magemonks_Menumanager_Model_Item
     */
    public function findParent($type)
    {
        if(!$this->getId()) return null;

        $parent = $this->getParent();
        while(!is_null($parent) && $parent->get('id')){
            if($parent->get('type') == $type) return $parent;
            $parent = $parent->getParent();
        }
        return null;
    }

    /**
     * Does the item have children?
     *
     * @param bool $includeHidden
     * @return bool
     */
    final public function hasChildren($includeHidden = false)
    {
        return count($this->getChildren($includeHidden)) > 0 ? true : false;
    }

    /**
     *  Adds a child to the _children array. And save the item under the parent if nessecary
     * @param Magemonks_Menumanager_Model_Item $item
     * @param null $position
     * @param bool $init
     * @return Magemonks_Menumanager_Model_Item
     */
    final public function addChild(Magemonks_Menumanager_Model_Item $item, $position = null, $init = false)
    {
        if($init === true){
            $this->children[] = $item;
        }
        else{
            $children = $this->getChildren(true);

            if($position > count($this->getChildren())){
                $position = count($this->getChildren());
            }

            if(!$item->getData('id')){
                //item has not yet been saved,
                $item->setData('parent_id', $this->getData('id'));
                $item->setData('menu_id', $this->getData('menu_id'));
                if(is_null($position)){
                    $position = count($children);
                }
                $item->setData('position', $position);
                $item->setData('level', $this->getData('level')+1);
                $save = true;
            }
            else{
                $save = false;
                if($item->getData('parent_id') != $this->getData('id')){
                    $item->setData('parent_id', $this->getData('id'));
                    $item->setData('level', $this->getData('level')+1);
                    $save = true;
                }
                if(!is_null($position)){
                    $item->setData('position', $position);
                    $save = true;
                }
            }
            if($save === true){
                $item->save();
            }
            $this->children[] = $item;
            uasort($this->children, function($a, $b){
                if($a->getData('position') == $b->getData('position'))return 0;
                return ($a->getData('position') < $b->getData('position')) ? -1 : 1;
            });

        }
        $item->parent = $this;

        return $this;
    }

    /**
     * Load object data
     *
     * @param   integer $id
     * @param   string $field
     * @return  Magemonks_Menumanager_Model_Item
     */
    final public function load($id, $field=null)
    {
        $item = $this;

        $item->_beforeLoad($id, $field);
        $item->_getResource()->load($this, $id, $field);
        $type = $item->getData('type');

        if(is_string($type) && !empty($type)){
            $modelName = 'menumanager/item_'.$type;
            $className = Mage::getConfig()->getModelClassName($modelName);
            $newItem = new $className();
            $newItem->setData($item->getData());

            $item = $newItem;
        }

        $item->_afterLoad();
        $item->setOrigData();
        $item->_hasDataChanges = false;

        return $item;
    }

    /**
     * Get's the base admin item form, used as a 'template' for all other forms
     *
     * @param null $fields
     * @return Magemonks_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = new Magemonks_Data_Form();

        //The element renderers have to be set explicitly
        Varien_Data_Form::setElementRenderer(
            Mage::app()->getLayout()->createBlock('adminhtml/widget_form_renderer_element')
        );
        Varien_Data_Form::setFieldsetRenderer(
            Mage::app()->getLayout()->createBlock('menumanager/adminhtml_widget_form_renderer_fieldset')
        );
        Varien_Data_Form::setFieldsetElementRenderer(
            Mage::app()->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset_element')
        );

        /* Item fieldset */
        $fieldset = $form->addFieldset('item_fieldset', array('legend'=>Mage::helper('menumanager')->__('Item'), 'class' => 'fieldset-wide'));

        /* Appearance fieldset */
        $fieldset = $form->addFieldset('appearance_fieldset', array('legend'=>Mage::helper('menumanager')->__('Appearance'), 'class' => 'fieldset-wide'));

        if($this->includeAdminFormFieldInForm('title', $fields)){
            $fieldset->addField('title', 'text', array(
                'name'      => 'title',
                'label'     => Mage::helper('menumanager')->__('Title [i18n]'),
                'title'     => Mage::helper('menumanager')->__('Title'),
                'note'      => Mage::helper('menumanager')->__('The title that will appear in the menu.'),
                'required'  => true,
            ));
        }

        if($this->includeAdminFormFieldInForm('sub_title', $fields)){
            $fieldset->addField('sub_title', 'text', array(
                'name'      => 'sub_title',
                'label'     => Mage::helper('menumanager')->__('Sub title [i18n]'),
                'title'     => Mage::helper('menumanager')->__('Sub title'),
                'note'      => Mage::helper('menumanager')->__('The subtitle that will appear in the menu (if your theme supports it).'),
                'required'  => false,
            ));
        }

        if($this->includeAdminFormFieldInForm('display_mode', $fields)){
            $fieldset->addField('display_mode', 'select', array(
                'name'      => 'display_mode',
                'label'     => Mage::helper('menumanager')->__('Display mode'),
                'title'     => Mage::helper('menumanager')->__('Display mode'),
                'note'      => Mage::helper('menumanager')->__('Select the display mode.'),
                'values'    => Mage::getModel('menumanager/source_displaymode')->toOptionArray(),
                'class'     => 'validate-select',
                'required'  => false,
            ));
        }

        if($this->includeAdminFormFieldInForm('title_color', $fields)){
            $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Colorpicker(array(
                'name'      => 'title_color',
                'label'     => Mage::helper('menumanager')->__('Title color'),
                'title'     => Mage::helper('menumanager')->__('Title color'),
                'note'      => Mage::helper('menumanager')->__('Select the title color'),
                'required'  => false,
            ));
            $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
            $field->setId('title_color');
            $fieldset->addElement($field);
        }

        if($this->includeAdminFormFieldInForm('image_url', $fields)){
            $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Mediapicker(array(
                'name'                  => 'image_url',
                'label'                 => Mage::helper('menumanager')->__('Image'),
                'title'                 => Mage::helper('menumanager')->__('Image'),
                'required'              => false,
                'note'                  => Mage::helper('menumanager')->__('Select the Image display in the menu.'),
            ));
            $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
            $field->setId('image_url');
            $fieldset->addElement($field);
        }

        if($this->includeAdminFormFieldInForm('image_alt', $fields)){
            $fieldset->addField('image_alt', 'text', array(
                'name'      => 'image_alt',
                'label'     => Mage::helper('menumanager')->__('Image alt [i18n]'),
                'title'     => Mage::helper('menumanager')->__('Image alt'),
                'note'      => Mage::helper('menumanager')->__('The ALT text for the image (if blank, the title will be used).'),
                'required'  => false,
            ));
        }

        if($this->includeAdminFormFieldInForm('image_width', $fields)){
            $fieldset->addField('image_width', 'text', array(
                'name'      => 'image_width',
                'label'     => Mage::helper('menumanager')->__('Image (max) Width'),
                'title'     => Mage::helper('menumanager')->__('Image (max) Width'),
                'note'      => Mage::helper('menumanager')->__('You can supply the width for the image. Based on you theme this will be used for the width or max-width.'),
                'required'  => false,
                'class'     => Mage::helper('magemonks')->isMageGte17() ? 'validate-not-negative-number' : '',
            ));
        }

        if($this->includeAdminFormFieldInForm('image_height', $fields)){
            $fieldset->addField('image_height', 'text', array(
                'name'      => 'image_height',
                'label'     => Mage::helper('menumanager')->__('Image (max) Height'),
                'title'     => Mage::helper('menumanager')->__('Image (max Height'),
                'note'      => Mage::helper('menumanager')->__('You can supply the height for the image. Based on you theme this will be used for the height or max-height.'),
                'required'  => false,
                'class'     => Mage::helper('magemonks')->isMageGte17() ? 'validate-not-negative-number' : '',
            ));
        }


        /* Availability fieldset */
        $fieldset = $form->addFieldset('availability_fieldset', array('legend'=>Mage::helper('menumanager')->__('Availability'), 'class' => 'fieldset-wide'));

        //note: the name customer_group_ids conflicted witht the product rules, so renamed it to customers_group_ids
        $fieldset->addField('customers_group_ids', 'multiselect', array(
            'name'      => 'customers_group_ids',
            'label'     => Mage::helper('menumanager')->__('Customer Groups'),
            'title'     => Mage::helper('menumanager')->__('Customer Groups'),
            'note'      => Mage::helper('menumanager')->__('Show only for the selected customer groups. Leave blank to show for all groups.'),
            'required'  => false,
            'values'    => Mage::getResourceModel('customer/group_collection')->toOptionArray(),
        ));



        $fieldset->addField('active_from', 'date', array(
            'name'      => 'active_from',
            'label'     => Mage::helper('menumanager')->__('Enabled from'),
            'title'     => Mage::helper('menumanager')->__('Enabled from'),
            'time'      => true,
            'format'    => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
            'image'     => Mage::getModel('core/design_package')->getSkinUrl('images/grid-cal.gif'),
            'note'      => Mage::helper('menumanager')->__('Set the date/time from when the menu item (including children) will be available. Leave empty for always.'),
            'required'  => false,
            'class'     => 'validate-date',
        ));



        $fieldset->addField('active_to', 'date', array(
            'name'      => 'active_to',
            'label'     => Mage::helper('menumanager')->__('Enabled to'),
            'title'     => Mage::helper('menumanager')->__('Enabled to'),
            'time'      => true,
            'format'    => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
            'image'     => Mage::getModel('core/design_package')->getSkinUrl('images/grid-cal.gif'),
            'note'      => Mage::helper('menumanager')->__('Set the date/time to when the menu item (including children) will be available. Leave empty for always.'),
            'required'  => false,
            'class'     => 'validate-date',
        ));

        /* HTML fieldset */
        $fieldset = $form->addFieldset('html_fieldset', array('legend'=>Mage::helper('menumanager')->__('HTML'), 'class' => 'fieldset-wide'));

        if($this->includeAdminFormFieldInForm('item_tag_id', $fields)){
            $fieldset->addField('item_tag_id', 'text', array(
                'name'      => 'item_tag_id',
                'label'     => Mage::helper('menumanager')->__('Item ID'),
                'title'     => Mage::helper('menumanager')->__('Item ID'),
                'note'      => Mage::helper('menumanager')->__('The ID of the item tag.'),
                'required'  => false,
                'class'     => 'validate-xml-identifier',
            ));
        }

        if($this->includeAdminFormFieldInForm('item_tag_class', $fields)){
            $fieldset->addField('item_tag_class', 'text', array(
                'name'      => 'item_tag_class',
                'label'     => Mage::helper('menumanager')->__('Item class'),
                'title'     => Mage::helper('menumanager')->__('Item class'),
                'note'      => Mage::helper('menumanager')->__('The (extra) CSS class(es) for the item tag.'),
                'required'  => false,
            ));
        }

        if($this->includeAdminFormFieldInForm('anchor_tag_class', $fields)){
            $fieldset->addField('anchor_tag_class', 'text', array(
                'name'      => 'anchor_tag_class',
                'label'     => Mage::helper('menumanager')->__('Anchor class'),
                'title'     => Mage::helper('menumanager')->__('Anchor class'),
                'note'      => Mage::helper('menumanager')->__('The (extra) CSS class(es) for the anchor tag'),
                'required'  => false,
            ));
        }

        if($this->includeAdminFormFieldInForm('anchor_tag_id', $fields)){
            $fieldset->addField('anchor_tag_id', 'text', array(
                'name'      => 'anchor_tag_id',
                'label'     => Mage::helper('menumanager')->__('Anchor ID'),
                'title'     => Mage::helper('menumanager')->__('Anchor ID'),
                'note'      => Mage::helper('menumanager')->__('The ID of the anchor tag.'),
                'required'  => false,
                'class'     => 'validate-xml-identifier',
            ));
        }

        if($this->includeAdminFormFieldInForm('anchor_tag_target', $fields)){
            $fieldset->addField('anchor_tag_target', 'text', array(
                'name'      => 'target',
                'label'     => Mage::helper('menumanager')->__('Anchor Target'),
                'title'     => Mage::helper('menumanager')->__('Anchor Target'),
                'note'      => Mage::helper('menumanager')->__('Set the target of the link (example: _blank). By default a link will open in the same window.'),
                'required'  => false,
            ));
        }

        if($this->includeAdminFormFieldInForm('anchor_tag_rel', $fields)){
            $fieldset->addField('anchor_tag_rel', 'text', array(
                'name'      => 'rel',
                'label'     => Mage::helper('menumanager')->__('Anchor Rel'),
                'title'     => Mage::helper('menumanager')->__('Anchor Rel'),
                'note'      => Mage::helper('menumanager')->__('The rel attribute of the anchor tag (example: nofollow).'),
                'required'  => false,
            ));
        }


        if($this->includeAdminFormFieldInForm('submenu_tag_class', $fields)){
            $fieldset->addField('submenu_tag_class', 'text', array(
                'name'      => 'submenu_tag_class',
                'label'     => Mage::helper('menumanager')->__('Submenu class'),
                'title'     => Mage::helper('menumanager')->__('Submenu  class'),
                'note'      => Mage::helper('menumanager')->__('The (extra) CSS class(es) for the sub-menu tag.'),
                'required'  => false,
            ));
        }

        if($this->includeAdminFormFieldInForm('submenu_tag_id', $fields)){
            $fieldset->addField('submenu_tag_id', 'text', array(
                'name'      => 'submenu_tag_id',
                'label'     => Mage::helper('menumanager')->__('Submenu ID'),
                'title'     => Mage::helper('menumanager')->__('Submenu ID'),
                'note' =>Mage::helper('menumanager')->__('The ID of the sub-menu tag.'),
                'required'  => false,
                'class'     => 'validate-xml-identifier',
            ));
        }

        $fieldset->addField('html_before', 'textarea', array(
            'name'      => 'html_before',
            'label'     => Mage::helper('menumanager')->__('HTML before'),
            'title'     => Mage::helper('menumanager')->__('HTML before'),
            'note'      => Mage::helper('menumanager')->__('HTML to render before the item wrapper .'),
            'required'  => false,
        ));


        $fieldset->addField('html_after', 'textarea', array(
            'name'      => 'html_after',
            'label'     => Mage::helper('menumanager')->__('HTML after'),
            'title'     => Mage::helper('menumanager')->__('HTML after'),
            'note'      => Mage::helper('menumanager')->__('HTML to render after the item wrapper .'),
            'required'  => false,
        ));


        $fieldset = $form->addFieldset('advanced_fieldset', array('legend'=>Mage::helper('menumanager')->__('Advanced'), 'class' => 'fieldset-wide'));


        if($this->includeAdminFormFieldInForm('partial_url_match', $fields)){
        $fieldset->addField('partial_url_match', 'select', array(
            'name'      => 'custom_route',
            'label'     => Mage::helper('menumanager')->__('Partial URL Match'),
            'title'     => Mage::helper('menumanager')->__('Partial URL Match'),
            'note'      => Mage::helper('menumanager')->__('Show active even if only part of the item\'s URL matches. For example, when set active "/foo" will still match if the URL is "/foo/bar", or "/foo?bar=true"'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'class'     => 'validate-select',
            'required'  => true,
        ));
        }
        $fieldset->addField('custom_template', 'text', array(
            'name'      => 'custom_template',
            'label'     => Mage::helper('menumanager')->__('Custom template'),
            'title'     => Mage::helper('menumanager')->__('Custom template'),
            'note'      => Mage::helper('menumanager')->__('Custom template (path) used to render the item (example: foo/bar.phtml).'),
            'required'  => false,
        ));
        $fieldset->addField('custom_block', 'text', array(
            'name'      => 'custom_block',
            'label'     => Mage::helper('menumanager')->__('Custom block'),
            'title'     => Mage::helper('menumanager')->__('Custom block'),
            'note'      => Mage::helper('menumanager')->__('Custom block (path) used to render the item (example: foo/bar).'),
            'required'  => false,
        ));


        return $form;
    }

    /**
     * Should the field be included in the admin form?
     *
     * @param $field
     * @param $fields
     * @return bool
     */
    public function includeAdminFormFieldInForm($field, $fields)
    {
        return (!is_array($fields) || is_array($fields) && in_array($field, $fields));
    }

    /**
     * Can the item be renamed?
     *
     * @return bool
     */
    public function canRename()
    {
        return true;
    }

    /**
     * Can the item be deleted?
     *
     * @return bool
     */
    public function canDelete()
    {
        return true;
    }

    /**
     * Can the item be enabled?
     *
     * @return bool
     */
    public function canEnable()
    {
        if($this->getData('is_active') == 1){
            return false;
        }
        else{
            return $this->canDisplay();
        }
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        return true;
    }

    /**
     * Can the item be disabled?
     *
     * @return bool
     */
    public function canDisable(){
        if($this->getData('is_active') == 1)
        {
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Get base path for the icons
     *
     * @return string
     */
    public function getAdminBaseIconPath()
    {
        return Mage::getDesign()->getSkinUrl('magemonks/menumanager/assets/icons');
    }

    /**
     * Get the path for the icon file
     *
     * @return string
     */
    public function getAdminIconPath()
    {
        $iconFile = isset($this->iconFile) ? $this->iconFile : 'folder.png';

        return $this->getAdminBaseIconPath().DS.$iconFile;
    }

    /**
     * Get the creation label
     *
     * @return string
     */
    public function getCreationLabel()
    {
        return $iconFile = isset($this->creationLabel) ? $this->creationLabel : 'Item';
    }

    /**
     * Get the admin context menu (used by javascript)
     *
     * @return mixed
     */
    public function getAdminContextMenu()
    {
        $helper = Mage::helper('magemonks');

        $json = $helper->getJsonWrapper(array(
            "canRename" => $this->canRename(),
            "canDelete" => $this->canDelete(),
            "canEnable" => $this->canEnable(),
            "canDisable" => $this->canDisable(),
            "isEnabled" => $this->getData('is_active'),
            "iconBasePath" => $this->getAdminBaseIconPath()
        ));

        $createMenu = array();

        $allowedChildren = $this->getAllowedChildren();
        foreach($allowedChildren as $child){
            if($child->canSave($this)){
                $createMenu[] = $helper->getJsonWrapper(array(
                    "type" => $child->getData('type'),
                    "label" => $child->getCreationLabel(),
                    "icon" => $child->getAdminIconPath()
                ));
            }
        }

        $json->createmenu = $createMenu;

        return $json;
    }

    /**
     * Get the admin types configuration (used by javascript)
     *
     * @return mixed
     */
    public function getAdminTypesConfiguration()
    {
        $helper = Mage::helper('magemonks');
        $json = $helper->getJsonWrapper(array(
            "max_children" => $this->maxChildren,
            "icon" => $helper->getJsonWrapper(array(
                "image" => $this->getAdminIconPath()
            ))
        ));
        $validChildren = array();
        $allowedChildren = $this->getAllowedChildren();
        foreach($allowedChildren as $child){
            $validChildren[] = $child->getData('type');
        }

        $json->valid_children = $validChildren;

        return $json;
    }

    /**
     * Get the allowed children
     *
     * @return array
     */
    public function getAllowedChildren()
    {
        $allowedChildren = array();
        $modelPaths = $this->getMenu(true)->getAllowedItemTypes();
        foreach($modelPaths as $modelPath){
            $item = Mage::getModel($modelPath);
            if($item->canBeAChildOf($this) && $this->canBeAParentOf($item)){
                $allowedChildren[] = $item;
            }
        }
        return $allowedChildren;
    }

    /**
     * Can the item be saved (under the given parent)
     *
     * @param null $newParent
     * @return bool
     */
    public function canSave($newParent = null)
    {
        if($newParent && $newParent->getData('level') != null){
            $level = $newParent->getData('level')+1;
        }
        else{
            $level = $this->getData('level');
        }

        $menu = null;
        if($this->getId()){
            $menu = $this->getMenu();
        }
        elseif($newParent && $newParent->getId()){
            $menu = $newParent->getMenu();
        }

        if($menu && is_numeric($menu->getData('max_child_depth')) && $menu->getData('max_child_depth') >= 0 && $level > $menu->getData('max_child_depth')){
            //Mage::log('canSave max_child_depth');
            return false;
        }

        if($this->maxDepth >= 0 && $level > $this->maxDepth){
            //Mage::log('canSave maxDepth');
            return false;
        }
        if($this->minDepth >= 0 && $level < $this->minDepth){
            //Mage::log('canSave minDepth');
            return false;
        }

        return true;
    }


    /**
     * Can the item be a parent of the given child?
     *
     * @param Magemonks_Menumanager_Model_Item $child
     * @return bool
     */
    public function canBeAParentOf(Magemonks_Menumanager_Model_Item $child)
    {
        if($this->connection == 'menu' && ($child->connection != 'menu' && $child->connection != 'menu-megamenu')){
            //Mage::log('canBeAParentOf menu');
            return false;
        }
        if($this->connection == 'megamenu' && ($child->connection != 'megamenu' && $child->connection != 'megamenu-menu')){
            //Mage::log('canBeAParentOf megamenu');
            return false;
        }
        if($this->connection == 'menu-megamenu' && $child->connection != 'megamenu'){
            //Mage::log('canBeAParentOf menu-megamenu');
            return false;
        }
        if($this->connection == 'megamenu-menu' && $child->connection != 'menu'){
            //Mage::log('canBeAParentOf megamenu-menu');
            return false;
        }

        if($this->getId()){
            //check parents max children
            if($this->maxChildren >= 0){
                if( ( !$child->getId() || ( $child->getId() && $child->getOrigData('parent_id') != $this->getId ) )  &&  count($this->getChildren())  >= $this->maxChildren ){
                    //Mage::log('canBeAParentOf maxChilden + id');
                    return false;
                }
            }
        }

        if(!$this->getId()){
            //check parents max children
            if($this->maxChildren === 0){
                //Mage::log('canBeAParentOf maxChildren - id');
                return false;
            }
        }

        $allowed = false;
        foreach($this->allowedChildTypes as $type){
            if($type === $child->getData('type') || $type === "all") $allowed = true;
        }

        if($allowed == false){
            ////Mage::log('canBeAParentOf types');
        }
        return $allowed;
    }

    /**
     * Can the item be a child of the given parent?
     *
     * @param Magemonks_Menumanager_Model_Item $parent
     * @return bool
     */
    public function canBeAChildOf(Magemonks_Menumanager_Model_Item $parent)
    {
        if($this->connection == 'menu' && ($parent->connection != 'menu' && $parent->connection != 'megamenu-menu')){
            //Mage::log('canBeAChildOf menu');
            return false;
        }
        if($this->connection == 'megamenu' && ($parent->connection != 'megamenu' && $parent->connection != 'menu-megamenu')){
            //Mage::log('canBeAChildOf megamenu');
            return false;
        }
        if($this->connection == 'menu-megamenu' && $parent->connection != 'menu'){
            //Mage::log('canBeAChildOf menu-megamenu');
            return false;
        }
        if($this->connection == 'megamenu-menu' && $parent->connection != 'megamenu'){
            //Mage::log('canBeAChildOf megamenu-menu');
            return false;
        }

        if($parent->getId()){
            if($this->maxSiblings >=0){
                if(( !$this->getId() || ($this->getId() && $this->getOrigData('parent_id') != $parent->getId()) ) &&  count($parent->getChildren()) > $this->maxSiblings){
                    //Mage::log('canBeAChildOf maxSiblings');
                    return false;
                }
            }
            foreach($parent->getChildren() as $child){
                if($child->maxSiblings >=0){
                    if(( !$this->getId() || ($this->getId() && $this->getOrigData('parent_id') != $parent->getId()) ) &&  count($parent->getChildren()) > $child->maxSiblings){
                        //Mage::log('canBeAChildOf siblings maxSiblings');
                        return false;
                    }
                }
            }
        }

        $allowed = false;
        foreach($this->allowedParentTypes as $type){
            if($type === $parent->getData('type') || $type === "all") $allowed = true;
        }
        foreach($this->disAllowedParentTypes as $type){
            if($type === $parent->getData('type')) $allowed = false;
        }
        if($allowed == false){
            ////Mage::log('canBeAChildOf type');
        }
        return $allowed;
    }

    /**
     * Is the item active?
     *
     * @return mixed
     */
    public function isActive()
    {
        return $this->getData('is_active');
    }

    /**
     * Get the block(s)
     *
     * @param null|Magemonks_Menumanager_Block_Item $parent
     * @return Magemonks_Menumanager_Block_Item[]
     */
    public function getBlocks(Magemonks_Menumanager_Block_Item $parent = null){
        $block = $this->_getBlockInstance($parent);
        if($block) return array($block);
        return array();
    }

    /**
     * Get a block instance
     *
     * @param null | Magemonks_Menumanager_Block_Item $parent
     * @return Magemonks_Menumanager_Block_Item
     */
    protected function _getBlockInstance(Magemonks_Menumanager_Block_Item $parent = null)
    {
        $blockPath = $this->get('custom_block', null);
        if(empty($blockPath)){
            $blockPath = $this->blockPath;
        }
        $template = $this->get('custom_template', null);
        if(empty($template)){
            $template = $this->template;
        }

        $block = Mage::app()->getLayout()->createBlock($blockPath, '', array(
            'template' => $template,
            'item' => $this,
            'parent' => $parent
        ));
        return $block;
    }

    /**
     * Do some magic with the image URL
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $image = $this->getConfiguration('image_url');
        if(!empty($image)){
            if(strpos($image,'/admin/cms_wysiwyg/directive/___directive/') !== false){
                $parts = explode('/',parse_url($image, PHP_URL_PATH));
                $key = array_search('___directive', $parts);
                if($key !== false){
                    $directive = $parts[$key+1];
                    $src = Mage::getModel('core/email_template_filter')->filter(Mage::helper('core')->urlDecode($directive));
                    if(!empty($src)){
                        $this->setConfiguration('image_url', parse_url($src, PHP_URL_PATH));
                    }
                }
            }
        }

        return parent::_beforeSave();
    }
}