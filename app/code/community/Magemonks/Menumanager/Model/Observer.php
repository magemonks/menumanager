<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Observer extends Mage_Core_Model_Abstract
{
    public function cleanCache($observer = null){
        Mage::getSingleton('core/cache')->clean(Magemonks_Menumanager_Model_Menu::CACHE_TAG);
    }

    /**
     * Clean cach when necesarry
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Magemonks_Menumanager_Model_Observer
     */
    public function itemScheduleCheck($schedule)
    {
        $items_from = Mage::getModel('menumanager/item')->getCollection()
                     ->addFieldToFilter('active_from', $schedule->getData('scheduled_at'));

        $items_to = Mage::getModel('menumanager/item')->getCollection()
            ->addFieldToFilter('active_to', $schedule->getData('scheduled_at'));

        if(count($items_from) || count($items_to)){
            $this->cleanCache();
        }

        return $this;
    }


    /**
     * Add the active and active-parent class to active menu items.
     * This is a workaround to prevent blocks from being fetched from cache without having active menu items.
     *
     * @param Varien_Event_Observer $observer
     */
    public function setActiveItemsHTML(Varien_Event_Observer $observer){
        Varien_Profiler::start('magemonks::menumanager::observer::set_active_items_html');

        $front = $observer->getData('front');
        $body = $front->getResponse()->getBody();


        $currentUrls = array();

        $currentUrl = Mage::getModel('core/url')->sessionUrlVar(Mage::helper('core/url')->getCurrentUrl());
        $pos = strpos($currentUrl, '?');
        if($pos){
            $currentUrl = substr($currentUrl, 0, $pos);
        }
        $currentUrls[] = $currentUrl;

        $category = Mage::registry('current_category');
        $product = Mage::registry('current_product');

        if($category && $product && $category->getId() && $product->getId()){
            $currentUrls[] = $category->getUrl();
        }

        $pattern = '@\<!-- start-menumanager-menu-(\d+) --\>(?:.*)<!-- stop-menumanager-menu-(?:\d+) --\>@isU';
        preg_match_all($pattern, $body, $menuParts, PREG_SET_ORDER);
        foreach($menuParts as $menuPart){
            $originalHtml = $html = $menuPart[0];
            $menuId = $menuPart[1];
            $menuData = $this->getMenuData($menuId);

            if(!is_array($menuData)) continue;

            $activeLinks = array();
            $activeParents = array();

            $pattern = '@\<a(?:(?:(?!\>).)*)href=[\'"](.*)[\'"]?\>@isU';
            preg_match_all($pattern, $originalHtml, $links, PREG_SET_ORDER);
            foreach($links as $link){
                foreach($currentUrls as $currentUrl){
                    if($menuData['partial_url_match'] == true && strpos($currentUrl, $link[1]) !== false){
                        array_push($activeLinks, $link[0]);
                    }
                    elseif($currentUrl === $link[1]){
                        array_push($activeLinks, $link[0]);
                    }
                }
            }

            foreach($activeLinks as $activeLink){
                //@todo: this could be done in one regular expression
                $pattern = '@nav[\d-]+@is';
                preg_match($pattern, $activeLink, $navClass);
                $navClass = end($navClass);

                $pattern = '#('.$navClass.'[\s|\'|"]{1})#';
                $html = preg_replace($pattern, "active $1", $html);

                while(strlen($navClass) >= 6){
                    $navClass = substr($navClass, 0, -2);
                    array_push($activeParents, $navClass);
                };
            }
            $activeParents = array_unique($activeParents);
            sort($activeParents);
            foreach($activeParents as $activeParent){
                $pattern = '#('.$activeParent.'[\s|\'|"]{1})#';
                $html = preg_replace($pattern, "active-parent $1", $html);
            }
            $html = str_replace(array('<!-- start-menumanager-menu-'.$menuId.' -->', '<!-- stop-menumanager-menu-'.$menuId.' -->'), '', $html);
            $body = str_replace($originalHtml, $html, $body);

        }
        Varien_Profiler::stop('magemonks::menumanager::observer::set_active_items_html');

        $front->getResponse()->setBody($body);
    }

    /**
     * Get the (cached) (orig) menu data
     *
     * @param $id
     * @return array|null
     */
    protected function getMenuData($id){
        if(false === Mage::app()->useCache(Magemonks_Menumanager_Model_Menu::CACHE_TAG)){
            $menu = Mage::getModel('menumanager/menu')->load($id);
            if(!$menu->getId()) return array();
            return $menu->getOrigData();
        }
        else{
            $cache = Mage::getSingleton('core/cache');
            $cacheId = 'MENUMANAGER_MENU_DATA_'.$id;
            $menuData = null;
            try{
                $menuData = unserialize($cache->load($cacheId));
            }
            catch(Exception $e){}
            if(!is_array($menuData)){
                $menu = Mage::getModel('menumanager/menu')->load($id);
                if(!$menu->getId()) return array();
                $menuData = $menu->getOrigData();
                $cache->save(serialize($menuData), $cacheId, array(
                    Magemonks_Menumanager_Model_Menu::CACHE_TAG,
                    Mage_Core_Model_Store::CACHE_TAG,
                    Mage_Core_Model_Store_Group::CACHE_TAG,
                    Mage_Cms_Model_Block::CACHE_TAG,
                    Mage_Catalog_Model_Category::CACHE_TAG,
                    Mage_Catalog_Model_Product::CACHE_TAG,
                    Mage_Catalog_Model_Product_Url::CACHE_TAG,
                    Mage_Catalog_Model_Product_Type_Price::CACHE_TAG,
                    Mage_Eav_Model_Entity_Attribute::CACHE_TAG,
                    Mage_Cms_Model_Block::CACHE_TAG,
                    Mage_Cms_Model_Page::CACHE_TAG,
                ));
            }
            return $menuData;
        }
    }
}