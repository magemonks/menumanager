<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Root extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array();
    public $iconFile = 'root.png';
    public $allowedParentTypes = array('');
    public $blockPath = "menumanager/item_root";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'root');
        $this->setData('label', 'Menu');
        $this->setData('level', 0);
        $this->setData('position', 0);
        $this->setData('parent_id', null);
        $this->setData('is_active', 1);
        $this->creationLabel = Mage::helper('menumanager')->__('Root');
    }

    /**
     * Return empty array for configuration fields
     *
     * @return array
     */
    public function getConfigurationFields()
    {
        return array();
    }

    /**
     * Return false for can disable
     *
     * @return bool
     */
    public function canDisable()
    {
        return false;
    }

    /**
     * Return false for can rename
     *
     * @return bool
     */
    public function canRename()
    {
        return false;
    }

    /**
     * Return false for can delete
     *
     * @return bool
     */
    public function canDelete()
    {
        return false;
    }

    /**
     * Return an empty form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        return new Varien_Data_Form();
        $form = parent::getAdminForm();
        return $form;
    }   
}