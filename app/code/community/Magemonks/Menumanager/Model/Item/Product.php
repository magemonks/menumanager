<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Product extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('product_id', 'out_of_stock');
    public $iconFile = 'product.png';
    public $blockPath = "menumanager/item_product";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'product');
        $this->creationLabel = Mage::helper('menumanager')->__('Product');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('display_mode', 'title', 'sub_title', 'title_color', 'image_url', 'image_alt', 'image_width', 'image_height', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'anchor_tag_id', 'item_tag_class', 'item_tag_id', 'submenu_tag_class', 'submenu_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Product'));

        $form->getElement('title')->setRequired(false)->setNote(Mage::helper('menumanager')->__('The text that will appear in the menu (leave blank to use the Product title instead).'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Gridselector_Product(array(
            'name'                  => 'product_id',
            'label'                 => Mage::helper('menumanager')->__('Product ID'),
            'title'                 => Mage::helper('menumanager')->__('Product ID'),
            'required'              => true,
            'note'                  => Mage::helper('menumanager')->__('Select the Product to be linked to.'),
            'class'                 => 'validate-not-negative-number',
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('product_id');
        $item_fieldset->addElement($field);

        $item_fieldset->addField('out_of_stock', 'select', array(
            'name'      => 'out_of_stock',
            'label'     => Mage::helper('menumanager')->__('When out of stock'),
            'title'     => Mage::helper('menumanager')->__('When out of stock'),
            'note'      => Mage::helper('menumanager')->__('How to handle out of stock items?'),
            'required'  => true,
            'values'    => array(
                ''              => Mage::helper('menumanager')->__('--Please Select--'),
                'hide'          => Mage::helper('menumanager')->__('Hide'),
                'show'          => Mage::helper('menumanager')->__('Show'),
            ),
            'class'     => 'validate-select',
        ));

        return $form;
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $product_id = $this->get('product_id');
        return !empty($product_id);
    }
}