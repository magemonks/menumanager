<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Multiproduct extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('rule', 'max_products', 'sort_by', 'sort_by_custom', 'sort_direction', 'out_of_stock');
    public $unusedAdminFormFields = array('title', 'sub_title');
    public $maxChildren = 0;
    public $iconFile = 'multiproduct.png';
    public $blockPath = "menumanager/item_multiproduct";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'multiproduct');
        $this->creationLabel = Mage::helper('menumanager')->__('Multi Product');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('title_color', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'item_tag_class', 'item_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Multi Product'));

        $item_fieldset->addField('max_products', 'text', array(
            'name'      => 'max_products',
            'label'     => Mage::helper('menumanager')->__('Maximum number of products'),
            'title'     => Mage::helper('menumanager')->__('Maximum number of products'),
            'note'      => Mage::helper('menumanager')->__('Enter the (maximum) number products to display'),
            'required'  => true,
            'class'     => 'validate-digits'
        ));

        $item_fieldset->addField('sort_by', 'select', array(
            'name'      => 'sort_by',
            'label'     => Mage::helper('menumanager')->__('Sort by'),
            'title'     => Mage::helper('menumanager')->__('Sort by'),
            'note'      => Mage::helper('menumanager')->__('Sorts the product by the given method.'),
            'required'  => true,
            'class'     => 'validate-select',
            'values'    => Mage::getModel('magemonks/source_productcollection_sortby')->toOptionArray(true, array('custom' => Mage::helper('menumanager')->__('-Custom-'),))
        ));

        $item_fieldset->addField('sort_by_custom', 'text', array(
            'name'      => 'sort_by_custom',
            'label'     => Mage::helper('menumanager')->__('Sort by (custom)'),
            'title'     => Mage::helper('menumanager')->__('Sort by (custom)'),
            'note'      => Mage::helper('menumanager')->__('Custom product attribute to sort by.'),
            'required'  => false,
            'class'     => 'validate-alphanum',

        ));

        $item_fieldset->addField('sort_direction', 'select', array(
            'name'      => 'sort_direction',
            'label'     => Mage::helper('menumanager')->__('Sort direction'),
            'title'     => Mage::helper('menumanager')->__('Sort direction'),
            'note'      => Mage::helper('menumanager')->__('Select the sort direction'),
            'required'  => true,
            'class'     => 'validate-select',
            'values'    => array(
                ''          => Mage::helper('menumanager')->__('--Please Select--'),
                'ASC'       => Mage::helper('menumanager')->__('Ascending'),
                'DESC'      => Mage::helper('menumanager')->__('Descending')
            )
        ));

        $item_fieldset->addField('out_of_stock', 'select', array(
            'name'      => 'out_of_stock',
            'label'     => Mage::helper('menumanager')->__('When out of stock'),
            'title'     => Mage::helper('menumanager')->__('When out of stock'),
            'note'      => Mage::helper('menumanager')->__('How to handle out of stock items?'),
            'required'  => true,
            'values'    => array(
                ''              => Mage::helper('menumanager')->__('--Please Select--'),
                'hide'          => Mage::helper('menumanager')->__('Hide'),
                'show'          => Mage::helper('menumanager')->__('Show'),
                'first'         => Mage::helper('menumanager')->__('Show, first'),
                'last'          => Mage::helper('menumanager')->__('Show, last'),
            ),
            'class'     => 'validate-select',
        ));

        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('promo/fieldset.phtml')
            ->setNewChildUrl(Mage::getModel('adminhtml/url')->getUrl('*/promo_catalog/newConditionHtml/form/item_conditions_fieldset'));

        $fieldset = $form->addFieldset('conditions_fieldset', array(
                'legend'=>Mage::helper('menumanager')->__('Product conditions (leave blank for all products)')), 'item_fieldset'
        )->setRenderer($renderer);

        $model = Mage::getModel('catalogrule/rule');
        $rule = $this->get('rule');
        if(is_array($rule)){
            $conditions = array('conditions' => $rule['conditions']);
            $model->loadPost($conditions);
        }

        $fieldset->addField('conditions', 'text', array(
            'name' => 'conditions',
            'label' => Mage::helper('menumanager')->__('Conditions'),
            'title' => Mage::helper('menumanager')->__('Conditions'),
            'required' => true,
        ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/conditions'));

        $model->getConditions()->setJsFormObject('item_conditions_fieldset');

        return $form;
    }

    /**
     * Before save
     *
     * Perform operations to save a rule in the database
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $rule = $this->getConfiguration('rule');
        if(is_array($rule)){
            $model = Mage::getModel('catalogrule/rule');
            $validateResult = $model->validateData(new Varien_Object($this->getConfiguration()));
            if ($validateResult !== true) {
                $errors = '';
                foreach($validateResult as $errorMessage) {
                    $errors .= $errorMessage.' ';
                }
                Mage::throwException($errors);
            }
        }

        return parent::_beforeSave();
    }

    /**
     * Get the blocks.
     *
     * @param null|Magemonks_Menumanager_Block_Item $parent
     * @return array
     */
    public function getBlocks(Magemonks_Menumanager_Block_Item $parent = null)
    {
        $blocks =  array();

        foreach($this->getProducts() as $product){
            $block = $this->_getBlockInstance($parent);
            $block->setData('item', $this);
            $block->setData('product_id', $product->getId());
            $blocks[] = $block;
        }
        return $blocks;
    }

    /**
     * Get the products (collection)
     *
     * @return mixed
     */
    public function getProducts()
    {
        $products = $this->getData('products');
        if(!$products){
            $collection_helper = Mage::helper('magemonks/productcollection');

            //Add the rule
            $ruleData = $this->get('rule');
            if(is_array($ruleData)){
                $conditions = array('conditions' => $ruleData['conditions']);
                $rule = Mage::getModel('catalogrule/rule')
                    ->loadPost($conditions);
                $collection_helper->addRule($rule);
            }

            if($this->get('out_of_stock') == 'hide'){
                $collection_helper->addFilter('hide_out_of_stock', null);
            }

            if($this->get('out_of_stock') == 'first'){
                $collection_helper->addSorting('has_stock', 'ASC');
            }

            if($this->get('out_of_stock') == 'last'){
                $collection_helper->addSorting('has_stock', 'DESC');
            }

            $sortBy = $this->get('sort_by');
            if($sortBy == 'custom'){
                $sortBy = $this->get('sort_by_custom');
            }
            $collection_helper->addSorting($sortBy, $this->get('sort_direction'));
            $collection_helper->setLimit($this->get('max_products'), 0);

            $collection = $collection_helper->getCollection();

            $this->setData('products', $collection);

        }
        return $this->getData('products');
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $max_products = $this->get('max_products');
        $sort_by = $this->get('sort_by');
        $sort_by_custom = $this->get('sort_by_custom');
        $sort_direction = $this->get('sort_direction');
        if(!empty($max_products) && !empty($sort_by) && !empty($sort_direction)){
            if($sort_by == 'custom'){
                return !empty($sort_by_custom);
            }
            return true;
        }
        return false;
    }
}