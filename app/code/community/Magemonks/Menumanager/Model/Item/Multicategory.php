<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Multicategory extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('category_id', 'excluded_category_ids', 'sort_by', 'sort_direction', 'max_child_depth');
    public $maxChildren = 0;
    public $iconFile = 'multicategory.png';
    public $blockPath = "menumanager/item_multicategory";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'multicategory');
        $this->creationLabel = Mage::helper('menumanager')->__('Multi Category');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('title_color', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'item_tag_class', 'item_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Multi Category'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Category_Select(array(
            'includeStoreRootCategory'  => true,
            'includeRootCategories'     => true,
            'name'                      => 'category_id',
            'label'                     => Mage::helper('menumanager')->__('Category'),
            'title'                     => Mage::helper('menumanager')->__('Category'),
            'note'                      => Mage::helper('menumanager')->__('Select the parent category of the categories to be displayed.'),
            'required'                  => true,
            'class'                     => 'validate-select',
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('category_id');
        $item_fieldset->addElement($field);

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Category_Multiselect(array(
            'includeStoreRootCategory'  => false,
            'includeRootCategories'     => false,
            'name'                      => 'excluded_category_ids',
            'label'                     => Mage::helper('menumanager')->__('Excluded categories'),
            'title'                     => Mage::helper('menumanager')->__('Excluded categories'),
            'note'                      => Mage::helper('menumanager')->__('Select the categories to be excluded. Note: The setting "Include in Navigation Menu" in the category settings (Catalog -> Manage Categories) is also respected.'),
            'required'                  => false,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('excluded_category_ids');
        $item_fieldset->addElement($field);

        $item_fieldset->addField('sort_by', 'select', array(
            'name'      => 'sort_by',
            'label'     => Mage::helper('menumanager')->__('Sort by'),
            'title'     => Mage::helper('menumanager')->__('Sort by'),
            'note'      => Mage::helper('menumanager')->__('Select the sort by'),
            'required'  => true,
            'class'     => 'validate-select',
            'values'    => array(
                ''                  => '',
                'position'          => Mage::helper('menumanager')->__('Position'),
                'name'              => Mage::helper('menumanager')->__('Category name')
            )
        ));

        $item_fieldset->addField('sort_direction', 'select', array(
            'name'      => 'sort_direction',
            'label'     => Mage::helper('menumanager')->__('Sort direction'),
            'title'     => Mage::helper('menumanager')->__('Sort direction'),
            'note'      => Mage::helper('menumanager')->__('Select the sort direction'),
            'required'  => true,
            'class'     => 'validate-select',
            'values'    => array(
                ''                  => '',
                'ASC'              => Mage::helper('menumanager')->__('Ascending'),
                'DESC'          => Mage::helper('menumanager')->__('Descending')
            )
        ));

        $item_fieldset->addField('max_child_depth', 'text', array(
            'name'      => 'max_child_depth',
            'label'     => Mage::helper('menumanager')->__('Maximum child depth'),
            'title'     => Mage::helper('menumanager')->__('Maximum child depth'),
            'note'      => Mage::helper('menumanager')->__('Enter the maximum child depth'),
            'required'  => false,
            'class'     => 'validate-not-negative-number',
        ));

        return $form;
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $category_id = $this->get('category_id');
        return !empty($category_id);
    }

    /**
     * Get the blocks.
     *
     * Since this is a MULTIcategory, it can output multiple blocks. To offer parent-child support,
     * A parent can be given as the parameter.
     *
     * @param null|Magemonks_Menumanager_Block_Item $parent
     * @return array
     */
    public function getBlocks(Magemonks_Menumanager_Block_Item $parent = null)
    {
        $rootCategoryId = Mage::getModel('magemonks/source_category')->getCategoryIdFromValue($this->get('category_id'));
        if(!$rootCategoryId) return array();
        $rootCategory =  Mage::getModel("catalog/category")->load($rootCategoryId);

        if($parent && $parent->getData('category') && $parent->getItem()->getData('type') === $this->getData('type')){
            $category = $parent->getData('category');
        }
        else{
            $category =  $rootCategory;
        }

        //Return empty array if the max child depth is reached
        $maxChildDepth = $this->get('max_child_depth');
        if(!empty($maxChildDepth)){
            if($category->getLevel() >= $rootCategory->getLevel() + $maxChildDepth) return array();
        }

        $excluded = (array) $this->get('excluded_category_ids');

        if (!$category->getId()) return array();
        if (in_array($category->getId(),$excluded)) return array();
        if (!$category->getIsActive()) return array();
        if (!$category->isInRootCategoryList() && Mage::app()->getStore()->getRootCategoryId() != $category->getId()) return array();

        $categories = $category->getChildrenCategories();
        $categories = $this->_sortCategories($categories, $this->get('sort_by'), $this->get('sort_direction'));

        $blocks = array();

        foreach($categories as $category){
            if (!$category->getId()) continue;
            if (in_array($category->getId(),$excluded)) continue;
            if (!$category->getIsActive()) continue;
            if (!$category->isInRootCategoryList() && Mage::app()->getStore()->getRootCategoryId() != $category->getId()) continue;

            $block = $this->_getBlockInstance($parent);
            $block->setData('item', $this);
            $block->setCategory($category);

            $blocks[] = $block;
        }

        return $blocks;
    }

    /**
     * Sort the categories by a given field (attribute)
     *
     * @param $categories
     * @param $field
     * @param $direction
     * @return array
     */
    protected function _sortCategories($categories, $field, $direction){
        if(!is_array($categories)){
            return $categories;
        }

        //categories are sorted by position by default
        if($field != 'position'){
            usort($categories, function($a, $b) use ($field){
                if($a->getData($field) == $b->getData($field)){
                    return 0;
                }
                return ($a->$field < $b->$field) ? -1 : 1;
            });
        }

        if(strtolower($direction) === 'desc'){
            $categories = array_reverse($categories);
        }

        return $categories;
    }
}