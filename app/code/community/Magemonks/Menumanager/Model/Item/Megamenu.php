<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('alignment', 'width');
    public $allowedChildTypes = array('megamenu_row', 'megamenu_header');
    public $disAllowedParentTypes = array('root');
    public $maxDepth = 2;
    public $minDepth = 2;
    public $maxSiblings = 0;
    public $iconFile = 'megamenu.png';
    public $blockPath = "menumanager/item_megamenu";
    public $template = "magemonks/menumanager/item/megamenu.phtml";
    public $connection = 'menu-megamenu';


    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu');
        $this->creationLabel = Mage::helper('menumanager')->__('Megamenu');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Megamenu'));

        $item_fieldset->addField('alignment', 'select', array(
            'name'      => 'alignment',
            'label'     => Mage::helper('menumanager')->__('Alignment'),
            'title'     => Mage::helper('menumanager')->__('Alignment'),
            'note'      => Mage::helper('menumanager')->__('Change the alignment of the mega menu.'),
            'required'  => true,
            'options'   => array(
                ''              => '',
                'left'          => Mage::helper('menumanager')->__('As far left as possible'),
                'right'         => Mage::helper('menumanager')->__('As far right as possible'),
                'aligned_item'   => Mage::helper('menumanager')->__('Aligned with the parent item'),
            ),
            'class'     => 'validate-select',
        ));

        $widthOptions = array();
        $maxColumns = $this->getMenu()->getData('megamenu_columns_per_row');
        $widthOptions[''] = '';
        if($maxColumns > 1) $widthOptions['fluid'] = Mage::helper('menumanager')->__('Fluid (with a maximum of %s columns)', $maxColumns);
        for($i=1; $i<=$maxColumns; $i++){
            if($i == 1) $widthOptions[$i] = Mage::helper('menumanager')->__('1 Column');
            else $widthOptions[$i] = Mage::helper('menumanager')->__('%s Columns', $i);
        }

        $item_fieldset->addField('width', 'select', array(
            'name'      => 'width',
            'label'     => Mage::helper('menumanager')->__('Width'),
            'title'     => Mage::helper('menumanager')->__('Width'),
            'note'      => Mage::helper('menumanager')->__('Set the width in of the Mega Menu.'),
            'required'  => true,
            'options'   => $widthOptions,
            'class'     => 'validate-select',
        ));


        return $form;
    }
}