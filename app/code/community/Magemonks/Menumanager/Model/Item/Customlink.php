<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Customlink extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('url');
    public $blockPath = "menumanager/item_customlink";
    public $iconFile = 'customlink.png';

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'customlink');
        $this->creationLabel = Mage::helper('menumanager')->__('Custom Link');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm('display_mode', 'title', 'sub_title', 'title_color', 'image_url', 'image_alt', 'image_width', 'image_height', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'anchor_tag_id', 'item_tag_class', 'item_tag_id', 'submenu_tag_class', 'submenu_tag_id', 'partial_url_match');

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Custom Link'));

        $item_fieldset->addField('url', 'text', array(
            'name'      => 'url',
            'label'     => Mage::helper('menumanager')->__('URL'),
            'title'     => Mage::helper('menumanager')->__('URL'),
            'note'      =>  Mage::helper('menumanager')->__('The URL'),
            'required'  => true,
        ));

        return $form;
    }

    /**
     * Can the item be displayed?
     * @return bool
     */
    public function canDisplay()
    {
        $url = $this->get('url');
        $title = $this->get('title');
        return !empty($url) && !empty($title);
    }
}