<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Customroute extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('custom_route', 'params');
    public $iconFile = 'customroute.png';
    public $blockPath = "menumanager/item_customroute";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'customroute');
        $this->creationLabel = Mage::helper('menumanager')->__('Custom Route');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('display_mode', 'title', 'sub_title', 'title_color', 'image_url', 'image_alt', 'image_width', 'image_height', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'anchor_tag_id', 'item_tag_class', 'item_tag_id', 'submenu_tag_class', 'submenu_tag_id', 'partial_url_match'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Custom route'));

        $item_fieldset->addField('custom_route', 'text', array(
            'name'      => 'custom_route',
            'label'     => Mage::helper('menumanager')->__('Route'),
            'title'     => Mage::helper('menumanager')->__('Route'),
            'note'      => Mage::helper('menumanager')->__('Use the Magento format module/controller/action'),
            'required'  => true,
        ));

        $item_fieldset->addField('params', 'textarea', array(
            'name'      => 'params',
            'label'     => Mage::helper('menumanager')->__('Route params'),
            'title'     => Mage::helper('menumanager')->__('Route params'),
            'note'      => Mage::helper('menumanager')->__('Use key value pairs, example: key=value. Use a new line for each pair.'),
            'required'  => false,
        ));

        return $form;
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $custom_route = $this->get('custom_route');
        $title = $this->get('title');
        return !empty($custom_route) && !empty($title);
    }
}