<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Magentopage extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('page_type');
    public $iconFile = 'magentopage.png';
    public $blockPath = "menumanager/item_magentopage";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'magentopage');
        $this->creationLabel = Mage::helper('menumanager')->__('Magento Page');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('display_mode', 'title', 'sub_title', 'title_color', 'image_url', 'image_alt', 'image_width', 'image_height', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'anchor_tag_id', 'item_tag_class', 'item_tag_id', 'submenu_tag_class', 'submenu_tag_id'));

        $form->getElement('title')->setRequired(false)->setNote(Mage::helper('menumanager')->__('The text that will appear in the menu (leave blank to use the default page title instead).'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Magento Page'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Magentopage(array(
            'name'      => 'page_type',
            'label'     => Mage::helper('menumanager')->__('Page'),
            'title'     => Mage::helper('menumanager')->__('Page'),
            'note'      => Mage::helper('menumanager')->__('Select Magento page'),
            'required'  => true,
            'class'     => 'validate-identifier',
        ));

        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('page_type');
        $item_fieldset->addElement($field, '^');

        return $form;
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $page_type = $this->get('page_type');
        return !empty($page_type);
    }

}