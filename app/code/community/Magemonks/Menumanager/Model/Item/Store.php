<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Store extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('store_string');
    public $iconFile = 'store.png';
    public $blockPath = "menumanager/item_store";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'store');
        $this->creationLabel = Mage::helper('menumanager')->__('Store');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('display_mode', 'title', 'sub_title', 'title_color', 'image_url', 'image_alt', 'image_width', 'image_height', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'anchor_tag_id', 'item_tag_class', 'item_tag_id', 'submenu_tag_class', 'submenu_tag_id'));

        $form->getElement('title')->setRequired(false)->setNote(Mage::helper('menumanager')->__('The text that will appear in the menu (leave blank to use the store title instead).'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Category'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Storeselector(array(
            'name'      => 'store_string',
            'label'     => Mage::helper('menumanager')->__('Store'),
            'title'     => Mage::helper('menumanager')->__('Store'),
            'note'      => Mage::helper('menumanager')->__('Select website or storegroup or store. Note: Only stores have URLs, so a webiste or storegroup will be resolved to their default store. Only the title might be different.'),
            'required'  => true,
            'class'     => 'validate-select',
        ));

        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('store_string');
        $item_fieldset->addElement($field, '^');

        return $form;
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $store_string = $this->get('store_string');
        return !empty($store_string);
    }

}