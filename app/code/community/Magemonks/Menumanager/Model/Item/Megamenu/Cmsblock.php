<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Cmsblock extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('cms_block_identifier');
    public $allowedParentTypes = array('megamenu_column');
    public $allowedChildTypes = array('');
    public $maxChildren = 0;
    public $iconFile = 'megamenu_cmsblock.png';
    public $blockPath = "menumanager/item_megamenu_cmsblock";
    public $template = "magemonks/menumanager/item/megamenu/cmsblock.phtml";
    public $connection = 'megamenu';

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_cmsblock');
        $this->creationLabel = Mage::helper('menumanager')->__('CMS Block');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('CMS Page'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Helper_Gridselector_Cmsblock(array(
            'name'      => 'cms_block_identifier',
            'label'     => Mage::helper('menumanager')->__('CMS Block'),
            'title'     => Mage::helper('menumanager')->__('CMS Block'),
            'required'  => true,
            'note'      =>  Mage::helper('menumanager')->__('Select the CMS Block to display in the menu.'),
            'class'     => 'validate-identifier',
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('cms_block_identifier');
        $item_fieldset->addElement($field, '^');

        return $form;
    }

    /**
     * Can the item be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $cms_block_identifier = $this->get('cms_block_identifier');
        return !empty($cms_block_identifier);
    }
}