<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Header extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('header_text', 'header_tag', 'header_tag_class');
    public $allowedParentTypes = array('megamenu_column', 'megamenu_row');
    public $allowedChildTypes = array('');
    public $iconFile = 'megamenu_header.png';
    public $connection = 'megamenu';
    public $blockPath = "menumanager/item_megamenu_header";
    public $template = "magemonks/menumanager/item/megamenu/header.phtml";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_header');
        $this->creationLabel = Mage::helper('menumanager')->__('Header');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Header'));

        $item_fieldset = $form->getElement('item_fieldset');

        $item_fieldset->addField('header_text', 'text', array(
            'name'      => 'header_text',
            'label'     => Mage::helper('menumanager')->__('Text'),
            'title'     => Mage::helper('menumanager')->__('text'),
            'note'      => Mage::helper('menumanager')->__('Set text of the header.'),
            'required'  => true,
        ));


        $recommended = 'h5';
        $parent = $this->getParent();
        if($parent && $parent->getId()){
            if($parent->getData('type') == 'megamenu_row'){
                $recommended = 'h5';
            }
            elseif($parent->getData('type') == 'megamenu_column'){
                $recommended = 'h6';
            }
        }
        $item_fieldset->addField('header_tag', 'select', array(
            'name'      => 'header_tag',
            'label'     => Mage::helper('menumanager')->__('Item tag'),
            'title'     => Mage::helper('menumanager')->__('Item tag'),
            'note'      => Mage::helper('menumanager')->__('Select the tag.'),
            'required'  => true,
            'options'   => array(
                ''          => '',
                'h1'        => 'h1'.($recommended == 'h1' ? ' '.Mage::helper('menumanager')->__('(recommended)') : ''),
                'h2'        => 'h2'.($recommended == 'h2' ? ' '.Mage::helper('menumanager')->__('(recommended)') : ''),
                'h3'        => 'h3'.($recommended == 'h3' ? ' '.Mage::helper('menumanager')->__('(recommended)') : ''),
                'h4'        => 'h4'.($recommended == 'h4' ? ' '.Mage::helper('menumanager')->__('(recommended)') : ''),
                'h5'        => 'h5'.($recommended == 'h5' ? ' '.Mage::helper('menumanager')->__('(recommended)') : ''),
                'h6'        => 'h6'.($recommended == 'h6' ? ' '.Mage::helper('menumanager')->__('(recommended)') : ''),
                'span'      => 'span'.($recommended == 'span' ? ' '.Mage::helper('menumanager')->__('(recommended)') : ''),
            ),
            'class'     => 'validate-select',
        ));

        $item_fieldset->addField('header_tag_class', 'text', array(
            'name'      => 'header_tag_class',
            'label'     => Mage::helper('menumanager')->__('Header CSS class(es)'),
            'title'     => Mage::helper('menumanager')->__('Header CSS class(es)'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Set the (extra) CSS class(es) for the header tag.'),
        ));

        return $form;
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $text =  $this->get('header_text');
        $tag =  $this->get('header_tag');

        if(!empty($text) && !empty($tag)){
            return true;
        }
        return false;
    }
}