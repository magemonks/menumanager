<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Menu extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('columnize');
    public $allowedParentTypes = array('megamenu_column');
    public $iconFile = 'megamenu_menu.png';
    public $connection = 'megamenu-menu';
    public $blockPath = "menumanager/item_megamenu_menu";
    public $template = "magemonks/menumanager/item/megamenu/menu.phtml";


    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_menu');
        $this->creationLabel = Mage::helper('menumanager')->__('Menu');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id', 'submenu_tag_class', 'submenu_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Menu'));

        $item_fieldset->addField('note', 'note', array(
            'name'      => 'information',
            'label'     => Mage::helper('menumanager')->__('Information'),
            'title'     => Mage::helper('menumanager')->__('Information'),
            'text'     => Mage::helper('menumanager')->__('This is a wrapper for normal menu items.')
        ));

        $item_fieldset->addField('columnize', 'select', array(
            'name'      => 'columnize',
            'label'     => Mage::helper('menumanager')->__('Columnize'),
            'title'     => Mage::helper('menumanager')->__('Columnize'),
            'note'     => Mage::helper('menumanager')->__('Order the items in columns, rather than rows. This will improve readability for large lists.'),
            'values'   => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'required'  =>  true,
            'class'     => 'validate-select',
        ));



        return $form;
    }
}