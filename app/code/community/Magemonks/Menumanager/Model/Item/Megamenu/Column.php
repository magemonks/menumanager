<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Column extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('width', 'border_top', 'border_left', 'border_bottom', 'border_right');
    public $allowedParentTypes = array('megamenu_row');
    public $iconFile = 'megamenu_column.png';
    public $connection = 'megamenu';
    public $blockPath = "menumanager/item_megamenu_column";
    public $template = "magemonks/menumanager/item/megamenu/column.phtml";


    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_column');
        $this->creationLabel = Mage::helper('menumanager')->__('Column');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Column'));

        $item_fieldset = $form->getElement('item_fieldset');

        $widthOptions = array();
        $maxColumns = $this->getMenu()->getData('megamenu_columns_per_row');
        $widthOptions[''] = '';
        for($i=1; $i<=$maxColumns; $i++){
            if($i == 1) $widthOptions[$i] = Mage::helper('menumanager')->__('1 Column');
            else $widthOptions[$i] = Mage::helper('menumanager')->__('%s Columns', $i);
        }

        $item_fieldset->addField('width', 'select', array(
            'name'      => 'width',
            'label'     => Mage::helper('menumanager')->__('Width'),
            'title'     => Mage::helper('menumanager')->__('Width'),
            'note'      => Mage::helper('menumanager')->__('Set the width in off the Mega Menu.'),
            'required'  => true,
            'options'   => $widthOptions,
            'class'     => 'validate-select',
        ));

        $appearance_fieldset = $form->getElement('appearance_fieldset');

        $appearance_fieldset->addField('border_top', 'select', array(
            'name'      => 'border_top',
            'label'     => Mage::helper('menumanager')->__('Border Top'),
            'title'     => Mage::helper('menumanager')->__('Border Top'),
            'note'      => Mage::helper('menumanager')->__('Adds a border to the top off the column.'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'class'     => 'validate-select',
        ));
        $appearance_fieldset->addField('border_bottom', 'select', array(
            'name'      => 'border_bottom',
            'label'     => Mage::helper('menumanager')->__('Border Bottom'),
            'title'     => Mage::helper('menumanager')->__('Border Bottom'),
            'note'      => Mage::helper('menumanager')->__('Adds a border to the bottom off the column.'),
            'values'   => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'class'     => 'validate-select',
        ));
        $appearance_fieldset->addField('border_left', 'select', array(
            'name'      => 'border_left',
            'label'     => Mage::helper('menumanager')->__('Border Left'),
            'title'     => Mage::helper('menumanager')->__('Border Left'),
            'note'      => Mage::helper('menumanager')->__('Adds a border to the left side off the column.'),
            'values'   => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'class'     => 'validate-select',
        ));
        $appearance_fieldset->addField('border_right', 'select', array(
            'name'      => 'border_right',
            'label'     => Mage::helper('menumanager')->__('Border Right'),
            'title'     => Mage::helper('menumanager')->__('Border Right'),
            'note'      => Mage::helper('menumanager')->__('Adds a border to the right side off the column.'),
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'class'     => 'validate-select',
        ));





        return $form;
    }

    /**
     * Can the item be displayed?
     * @return bool
     */
    public function canDisplay()
    {
        $width = $this->getConfiguration('width');
        return !empty($width);
    }
}