<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Image extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('image_url', 'image_alt', 'image_width', 'image_height', 'url');
    public $allowedParentTypes = array('megamenu_column');
    public $blockPath = "menumanager/item_megamenu_image";
    public $template = "magemonks/menumanager/item/megamenu/image.phtml";
    public $iconFile = 'megamenu_image.png';
    public $connection = 'megamenu';

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_image');
        $this->creationLabel = Mage::helper('menumanager')->__('Image');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('image_url', 'image_alt', 'image_width', 'image_height', 'item_tag_class', 'item_tag_id', 'anchor_tag_target', 'anchor_tag_rel', 'anchor_tag_class', 'anchor_tag_id',));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Image'));
        $appearance_fieldset = $form->getElement('appearance_fieldset');

        $moveElements = array('image_url' => true, 'image_alt' => true, 'image_width' => false, 'image_height' => false);
        foreach($moveElements as $moveElement => $required){
            $element = $form->getElement($moveElement);
            $element->setRequired($required);

            $appearance_fieldset->removeField($moveElement);
            $item_fieldset->addElement($element);
        }

        return $form;
    }

    /**
     * Can the item be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $image_url = $this->get('image_url');
        return !empty($image_url);
    }

}