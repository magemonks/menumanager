<?php
/**
 * Magemonks
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package is designed for Magento COMMUNITY edition
 * Magemonks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Magemonks does not provide support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @copyright   Copyright (c) 2012 Magemonks. (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 */

/**
 * Magemonks Magemonks_Menumanager_Model_Item_Megamenu
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 */

class Magemonks_Menumanager_Model_Item_Megamenu_Row extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('border_top', 'border_bottom');
    public $allowedParentTypes = array('megamenu');
    public $allowedChildTypes = array('megamenu_column', 'megamenu_header');
    public $connection = 'megamenu';
    public $iconFile = 'megamenu_row.png';
    public $blockPath = "menumanager/item_megamenu_row";
    public $template = "magemonks/menumanager/item/megamenu/row.phtml";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_row');
        $this->creationLabel = Mage::helper('menumanager')->__('Row');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        $appearance_fieldset = $form->getElement('appearance_fieldset');

        $appearance_fieldset->addField('border_top', 'select', array(
            'name'          => 'border_top',
            'label'         => Mage::helper('menumanager')->__('Border Top'),
            'title'         => Mage::helper('menumanager')->__('Border Top'),
            'note'      => Mage::helper('menumanager')->__('Adds a border to the top off the row.'),
            'values'   => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'class'     => 'validate-select',
        ));
        $appearance_fieldset->addField('border_bottom', 'select', array(
            'name'      => 'border_bottom',
            'label'     => Mage::helper('menumanager')->__('Border Bottom'),
            'title'     => Mage::helper('menumanager')->__('Border Bottom'),
            'note'      => Mage::helper('menumanager')->__('Adds a border to the bottom off the row.'),
            'values'   => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
            'class'     => 'validate-select',
        ));

        return $form;
    }
}