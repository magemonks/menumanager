<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Productsblock extends Magemonks_Menumanager_Model_Item_Multiproduct
{
    public $allowedParentTypes = array('megamenu_column');
    public $allowedChildTypes = array('');
    public $maxChildren = 0;
    public $iconFile = 'megamenu_productsblock.png';
    public $connection = 'megamenu';
    public $blockPath = "menumanager/item_megamenu_productsblock";
    public $template = "magemonks/menumanager/item/megamenu/productsblock.phtml";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_productsblock');
        $this->creationLabel = Mage::helper('menumanager')->__('Products Block');
        array_push($this->configFields, 'width', 'elements');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm();

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Products Block'));

        $widthOptions = array();
        $parent = $this->getParent();
        $maxColumns = null;
        if($parent){
            $maxColumns = $this->getParent()->get('width');
        }
        if(empty($maxColumns)){
            $widthOptions[''] = Mage::helper('menumanager')->__('Define the parent width first');
        }
        else{
            $widthOptions[''] = '';
            for($i=1; $i<=$maxColumns; $i++){
                    if($i == 1) $widthOptions[$i] = Mage::helper('menumanager')->__('1 Column');
                    else $widthOptions[$i] = Mage::helper('menumanager')->__('%s Columns', $i);
            }
        }
        $item_fieldset->addField('width', 'select', array(
            'name'      => 'width',
            'label'     => Mage::helper('menumanager')->__('Width'),
            'title'     => Mage::helper('menumanager')->__('Width'),
            'note'      => Mage::helper('menumanager')->__('Set the width per block.'),
            'required'  => true,
            'options'   => $widthOptions,
            'class'     => 'validate-select',
        ), '^');

        $item_fieldset->addField('elements', 'multiselect', array(
            'name'      => 'elements',
            'label'     => Mage::helper('menumanager')->__('Elements'),
            'title'     => Mage::helper('menumanager')->__('Elements'),
            'note'      => Mage::helper('menumanager')->__('Choose at least one element to display. Hold ctrl (cmd on a mac) to select more than one)'),
            'required'  => true,
            'values'   => array(
                array('value' => 'image', 'label' => Mage::helper('menumanager')->__('Image')),
                array('value' => 'name', 'label' => Mage::helper('menumanager')->__('Name')),
                array('value' => 'reviews', 'label' => Mage::helper('menumanager')->__('Reviews')),
                array('value' => 'price', 'label' => Mage::helper('menumanager')->__('Price')),
                array('value' => 'add_to_cart', 'label' => Mage::helper('menumanager')->__('Add to cart')),
                array('value' => 'out_of_stock', 'label' => Mage::helper('menumanager')->__('Out of stock')),
                array('value' => 'short_description', 'label' => Mage::helper('menumanager')->__('Short description')),
                array('value' => 'learn_more', 'label' => Mage::helper('menumanager')->__('Learn more')),
                array('value' => 'add_to_wishlist', 'label' => Mage::helper('menumanager')->__('Add to wishlist')),
                array('value' => 'add_to_compare', 'label' => Mage::helper('menumanager')->__('Add to compare')),
                array('value' => 'extra_1', 'label' => Mage::helper('menumanager')->__('Extra 1 (define in template)')),
                array('value' => 'extra_2', 'label' => Mage::helper('menumanager')->__('Extra 2 (define in template)')),
            ),
        ));

        return $form;
    }

}