<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Horizontaldivider extends Magemonks_Menumanager_Model_Item
{
    public $allowedParentTypes = array('megamenu_column');
    public $iconFile = 'megamenu_horizontaldivider.png';
    public $connection = 'megamenu';
    public $blockPath = "menumanager/item_megamenu_horizontaldivider";
    public $template = "magemonks/menumanager/item/megamenu/horizontaldivider.phtml";

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_horizontaldivider');
        $this->creationLabel = Mage::helper('menumanager')->__('Horizontal divider');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        return $form;
    }

    /**
     * Can the item be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        return true;
    }
}