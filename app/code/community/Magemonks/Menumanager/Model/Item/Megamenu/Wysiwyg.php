<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Wysiwyg extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('content', 'remove_outer_p_tags');
    public $allowedParentTypes = array('megamenu_column');
    public $allowedChildTypes = array();
    public $blockPath = "menumanager/item_megamenu_wysiwyg";
    public $template = "magemonks/menumanager/item/megamenu/wysiwyg.phtml";
    public $iconFile = 'megamenu_wysiwyg.png';
    public $connection = 'megamenu';

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_wysiwyg');
        $this->creationLabel = Mage::helper('menumanager')->__('WYSIWYG Block');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('WYSIWYG Block'));

        $field = new Magemonks_Core_Block_Adminhtml_Form_Element_Wysiwyg(array(
            'name'      => 'content',
            'label'     => Mage::helper('menumanager')->__('Content'),
            'title'     => Mage::helper('menumanager')->__('Content'),
            'required'  => true,
        ));
        $field->setRenderer(Varien_Data_Form::getFieldsetElementRenderer());
        $field->setId('content');
        $item_fieldset->addElement($field);

        $item_fieldset->addField('remove_outer_p_tags', 'select', array(
            'name'      => 'remove_outer_p_tags',
            'label'     => Mage::helper('menumanager')->__('Remove outer P tags'),
            'title'     => Mage::helper('menumanager')->__('Remove outer P tags'),
            'note'      => Mage::helper('menumanager')->__('The Magento WYSIWYG editor puts a P tag around the content. This options allows you to automatically strip these tags'),
            'required'  => false,
            'options'   => array(
                0 => Mage::helper('menumanager')->__('No'),
                1 => Mage::helper('menumanager')->__('Yes'),
            )
        ));

        return $form;
    }

    /**
     * Can the item be displayed?
     *
     * @return bool
     */
    public function canDisplay()
    {
        $content = $this->get('content');
        return !empty($content);
    }

}