<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Model_Item_Megamenu_Vimeo extends Magemonks_Menumanager_Model_Item
{
    public $configFields = array('embed_code');
    public $allowedParentTypes = array('megamenu_column');
    public $allowedChildTypes = array('');
    public $maxChildren = 0;
    public $blockPath = "menumanager/item_megamenu_vimeo";
    public $template = "magemonks/menumanager/item/megamenu/vimeo.phtml";
    public $iconFile = 'megamenu_vimeo.png';
    public $connection = 'megamenu';

    public function _construct()
    {
        parent::_construct();
        $this->setData('type', 'megamenu_vimeo');
        $this->creationLabel = Mage::helper('menumanager')->__('Vimeo');
    }

    /**
     * Get the admin form
     *
     * @param null $fields
     * @return Varien_Data_Form
     */
    public function getAdminForm($fields = null)
    {
        $form = parent::getAdminForm(array('item_tag_class', 'item_tag_id'));

        $item_fieldset = $form->getElement('item_fieldset')->setData('legend', Mage::helper('menumanager')->__('Vimeo'));

        $item_fieldset->addField('embed_code', 'textarea', array(
            'name'      => 'embed_code',
            'label'     => Mage::helper('menumanager')->__('Vimeo embed code'),
            'title'     => Mage::helper('menumanager')->__('Vimeo embed code'),
            'note'      => Mage::helper('menumanager')->__('Enter the Vimeo embed code (new style, with iframe). The width will be automatically ajusted to the column width. The height will be adjusted relative to the new width. All code outside the iframe tags is automatically removed.'),
            'required'  => true,
        ));

        return $form;
    }

    /**
     * Strip everything before <iframe and after </iframe>
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $embedCode = $this->getConfiguration('embed_code');
        if(!empty($embedCode)){
            if(strpos($embedCode,'<iframe') !== false){
                $embedCode = substr($embedCode, strpos($embedCode,'<iframe'));
            }
            if(strpos($embedCode,'</iframe>') !== false){
                $embedCode = substr($embedCode, 0, strpos($embedCode,'</iframe>')+9);
            }
            $this->setConfiguration('embed_code', $embedCode);
        }

        return parent::_beforeSave();
    }

    /**
     * Can the item be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $url = $this->get('embed_code');
        return !empty($url);
    }

}