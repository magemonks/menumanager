<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */

// ADD THE TOP NAVIGATION
$menudata = array(
        'label'                     => 'Main menu',
        'identifier'                => 'main_menu',
        'type'                      => "horizontal",
        'is_active'                 => 1,
        'stores'                    => array(0),
        'html_before'               => '<div class="nav-container">',
        'html_after'                => "</div>\r\n<script>\r\ndocument.observe(\"dom:loaded\", function() {\r\n mainNav(\"topnav\", {\"show_delay\":\"100\",\"hide_delay\":\"100\"});\r\n});\r\n</script>",
        'menu_tag'                  => 'ul',
        'menu_tag_id'               => 'topnav',
        'submenu_tag'               => 'ul',
        'itemu_tag'                 => 'li',
        'item_active_class'         => 'active',
        'item_active_parent_class'  => 'active',
        'megamenu_columns_per_row'  => 6,
        'megamenu_tag'              => 'div',
        'megamenu_item_tag'         => 'div',
);
$menu = Mage::getModel('menumanager/menu')->setData($menudata)->save();
$item = Mage::getModel('menumanager/item_multicategory')
            ->set('category_id', Magemonks_Core_Model_Source_Category::STORE_ROOT)
            ->set('label', 'Categories')
            ->set('sort_by', 'position')
            ->set('sort_direction', 'ASC')
            ->set('is_active', 1);

$menu->addItem($item, $menu->getRootItem(), 0);


// ADD THE TOP LINKS
$menudata = array(
    'label'                     => 'Header links',
    'identifier'                => 'header_links',
    'type'                      => "horizontal",
    'is_active'                 => 1,
    'stores'                    => array(0),
    'menu_tag'                  => 'ul',
    'menu_tag_class'            => 'links',
    'submenu_tag'               => 'ul',
    'itemu_tag'                 => 'li',
    'item_active_class'         => 'active',
    'megamenu_columns_per_row'  => 1,
    'megamenu_tag'              => 'div',
    'megamenu_item_tag'         => 'div',
    'max_child_depth'           => 1
);
$menu = Mage::getModel('menumanager/menu')->setData($menudata)->save();

$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'account')
    ->set('label', 'My account')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'wishlist')
    ->set('label', 'My wishlist')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'cart')
    ->set('label', 'My cart')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'checkout')
    ->set('label', 'Checkout')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'login')
    ->set('label', 'Login')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'logout')
    ->set('label', 'Logout')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());


// ADD THE FOOTER LINKS
$menudata = array(
    'label'                     => 'Footer links (top row)',
    'identifier'                => 'footer_links_top',
    'type'                      => "horizontal",
    'is_active'                 => 1,
    'stores'                    => array(0),
    'menu_tag'                  => 'ul',
    'menu_tag_class'            => 'links',
    'submenu_tag'               => 'ul',
    'itemu_tag'                 => 'li',
    'item_active_class'         => 'active',
    'megamenu_columns_per_row'  => 1,
    'megamenu_tag'              => 'div',
    'megamenu_item_tag'         => 'div',
    'max_child_depth'           => 1
);
$menu = Mage::getModel('menumanager/menu')->setData($menudata)->save();
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'sitemap')
    ->set('label', 'Sitemap')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'search_terms')
    ->set('label', 'Search Terms')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'advanced_search')
    ->set('label', 'Advanced Search')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'orders_and_returns')
    ->set('label', 'Orders and returns')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'contact_us')
    ->set('label', 'Contact Us')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());


// ADD THE CMS FOOTER LINKS
$menudata = array(
    'label'                     => 'Footer links (bottom row)',
    'identifier'                => 'footer_links_bottom',
    'type'                      => "horizontal",
    'is_active'                 => 1,
    'stores'                    => array(0),
    'menu_tag'                  => 'ul',
    'menu_tag_class'            => 'links',
    'submenu_tag'               => 'ul',
    'itemu_tag'                 => 'li',
    'item_active_class'         => 'active',
    'megamenu_columns_per_row'  => 1,
    'megamenu_tag'              => 'div',
    'megamenu_item_tag'         => 'div',
    'max_child_depth'           => 1
);
$menu = Mage::getModel('menumanager/menu')->setData($menudata)->save();

$item = Mage::getModel('menumanager/item_cmspage')
    ->set('cms_page_identifier', 'about-magento-demo-store')
    ->set('label', 'About us')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_cmspage')
    ->set('cms_page_identifier', 'customer-service')
    ->set('label', 'Customer service')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_cmspage')
    ->set('cms_page_identifier', 'privacy-policy-cookie-restriction-mode')
    ->set('label', 'Privacy policy')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());



// ADD CUSTOMER ACCOUNT NAVIGATION
$menudata = array(
    'label'                     => 'Customer Account Navigation',
    'identifier'                => 'customer_account_navigation',
    'type'                      => "vertical",
    'is_active'                 => 1,
    'stores'                    => array(0),
    'menu_tag'                  => 'ul',
    'menu_tag_class'            => 'links',
    'submenu_tag'               => 'ul',
    'itemu_tag'                 => 'li',
    'item_active_class'         => 'current',
    'megamenu_columns_per_row'  => 1,
    'megamenu_tag'              => 'div',
    'megamenu_item_tag'         => 'div',
    'max_child_depth'           => 1,
    'custom_template'           => 'magemonks/menumanager/menu/customer_account.phtml'
);
$menu = Mage::getModel('menumanager/menu')->setData($menudata)->save();
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'account_dashboard')
    ->set('label', 'Account Dashboard')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'account_information')
    ->set('label', 'Account Information')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'address_book')
    ->set('label', 'Address Book')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'orders')
    ->set('label', 'My Orders')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'billing_agreements')
    ->set('label', 'Billing Agreements')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'recurring_profiles')
    ->set('label', 'Recurring Profiles')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'product_reviews')
    ->set('label', 'My Product Reviews')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'tags')
    ->set('label', 'My Tags')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'wishlist')
    ->set('label', 'My Wishlist')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'applications')
    ->set('label', 'My Applications')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'newsletter_subscriptions')
    ->set('label', 'Newsletter Subscriptions')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());
$item = Mage::getModel('menumanager/item_magentopage')
    ->set('page_type', 'downloadable_products')
    ->set('label', 'Newsletter Subscriptions')
    ->set('is_active', 1);
$menu->addItem($item, $menu->getRootItem());