<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */


/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'menumanager/menu'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('menumanager/menu'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'ID')

    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TEXT, 25, array(
        'nullable'  => false,
        'default'   => 'horizontal',
        ), 'Menu Type')

    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Menu Label')

    ->addColumn('identifier', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'string identifier')

    ->addColumn('max_child_depth', Varien_Db_Ddl_Table::TYPE_TEXT, 5, array(
        'nullable'  => true,
        ), 'max child depth')

    ->addColumn('partial_url_match', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '0',
        ), 'Partial URL matching')

    ->addColumn('menu_tag', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        'default'   => 'ul',
        ), 'menu menu_tag')

    ->addColumn('menu_tag_class', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true,
        ), 'menu tag class')

    ->addColumn('menu_tag_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true,
        ), 'menu tag id')

    ->addColumn('html_before', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
        ), 'html before')

    ->addColumn('html_after', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
        ), 'html after')

    ->addColumn('submenu_tag', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        'default'   => 'ul',
        ), 'sub menu tag')

    ->addColumn('submenu_tag_class', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true,
        ), 'sub menu class')

    ->addColumn('item_tag', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        'default'   => 'li',
        ), 'item tag')

    ->addColumn('item_tag_class', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true,
        ), 'item tag class')

    ->addColumn('item_active_class', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
    ), 'item active class')

    ->addColumn('item_active_parent_class', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true,
    ), 'item active parent class')

    ->addColumn('megamenu_tag', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Maga menu item tag')

    ->addColumn('megamenu_tag_class', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Maga menu item tag class')

    ->addColumn('megamenu_item_tag', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Maga menu item tag')

    ->addColumn('megamenu_item_tag_class', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Maga menu item tag class')

    ->addColumn('megamenu_columns_per_row', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        ), 'maximum columns per row in a megamenu')

    ->addColumn('custom_template', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true,
        ), 'custom template')

    ->addColumn('disallowed_item_types', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
        ), 'disallowed item types')

    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'creation time')

    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'modification time')

    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
        ), 'Is menu active')

    ->setComment('Magemonks menumanager Menu Table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'menumanager/menu_store'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('menumanager/menu_store'))
    ->addColumn('menu_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Menu ID')

    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')

    ->addIndex($installer->getIdxName('menumanager/menu_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('menumanager/menu_store', 'menu_id', 'menumanager/menu', 'id'),
        'menu_id', $installer->getTable('menumanager/menu'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('menumanager/menu_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Magemonks menumanager Menu To Store Linkage Table');
$installer->getConnection()->createTable($table);



/**
 * Create table 'menumanager/item'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('menumanager/item'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'id')
    ->addColumn('menu_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        ), 'menu ID')

    ->addColumn('parent_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => true,
        ), 'parent ID')

    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        ), 'position')

    ->addColumn('level', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        ), 'level')

    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'label')

    ->addColumn('active_from', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'active from')

    ->addColumn('active_to', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'active to')

    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'creation time')

    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Modification time')

    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '0',
        ), 'Is Menu Active')

    ->addColumn('configuration', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
        ), 'configuration')

    ->addColumn('type', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'type')

    ->addForeignKey($installer->getFkName('menumanager/item', 'menu_id', 'menumanager/menu', 'id'),
        'menu_id', $installer->getTable('menumanager/menu'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('menumanager/item', 'parent_id', 'menumanager/item', 'id'),
        'parent_id', $installer->getTable('menumanager/item'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)        
    ->setComment('Magemonks menumanager Item Table');
$installer->getConnection()->createTable($table);

$installer->endSetup();
