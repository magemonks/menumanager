<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Adminhtml_Menu_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('menumanagerMenuGrid');
        $this->setDefaultSort('menu_identifier');
        $this->setDefaultDir('ASC');
    }

    /**
     * Prepare collection
     *
     * @return this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('menumanager/menu')->getCollection();
        /* @var $collection Magemonks_Menumanager_Model_Resource_Menu_Collection */
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('label', array(
            'header'    => Mage::helper('menumanager')->__('Label'),
            'align'     => 'left',
            'index'     => 'label',
        ));

        $this->addColumn('identifier', array(
            'header'    => Mage::helper('menumanager')->__('Identifier'),
            'align'     => 'left',
            'index'     => 'identifier'
        ));
        
        $this->addColumn('type', array(
            'header'    => Mage::helper('menumanager')->__('Type'),
            'align'     => 'left',
            'index'     => 'type',
            'type'      => 'options',
            'options'   => array(
                'horizontal'    => Mage::helper('menumanager')->__('Horizontal'),
                'vertical'      => Mage::helper('menumanager')->__('Vertical'),
            ),            
        ));        

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('menumanager')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('menumanager')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
                0 => Mage::helper('menumanager')->__('Disabled'),
                1 => Mage::helper('menumanager')->__('Enabled')
            ),
        ));

        $this->addColumn('creation_time', array(
            'header'    => Mage::helper('menumanager')->__('Date Created'),
            'index'     => 'creation_time',
            'type'      => 'datetime',
        ));

        $this->addColumn('update_time', array(
            'header'    => Mage::helper('menumanager')->__('Last Modified'),
            'index'     => 'update_time',
            'type'      => 'datetime',
        ));

        $this->addColumn('changeSettings', array(
            'header'    => Mage::helper('menumanager')->__('Settings'),
            'width'     => 10,
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'menumanager/adminhtml_menu_grid_render_action',
        ));

        return parent::_prepareColumns();
    }

    /**
     * After load collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        return parent::_afterLoadCollection();
    }

    /**
     * Filter on store
     *
     * @param $collection
     * @param $column
     * @return mixed
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }

    /**
     * Get the row URL
     *
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/menumanager_item/edit', array('id' => $row->getRootItem()->getId()));
    }

}
