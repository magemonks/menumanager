<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Adminhtml_Menu_Grid_Render_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
            'id'        => "changeSettings",
            'onclick'   => "setLocation('".$this->getUrl('*/menumanager_item/edit', array('id' => $row->getRootItem()->getId()))."')",
            'label'     => Mage::helper('menumanager')->__('Settings'),
            'class' => 'go'
        ));

        return '<a href="'.$this->getUrl('adminhtml/menumanager_menu/edit', array('id' => $row->getId())).'">'.$button->toHtml().'</a>';
    }
}
