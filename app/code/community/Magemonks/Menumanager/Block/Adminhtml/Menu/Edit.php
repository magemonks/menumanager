<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Adminhtml_Menu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_menu';
        $this->_blockGroup = 'menumanager';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('menumanager')->__('Save Menu'));
        $this->_updateButton('delete', 'label', Mage::helper('menumanager')->__('Delete Menu'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);



        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

        if (Mage::registry('menumanager_menu')->getId()) {
            $this->_addButton('edititems', array(
                'label'     => Mage::helper('adminhtml')->__('Edit Items'),
                'onclick'   => 'setLocation(\'' . $this->getUrl('*/menumanager_item/edit', array('id' => Mage::registry('menumanager_menu')->getRootItem()->getId())) . '\')',
                'class'     => 'go',
            ), -100, 100);
        }

    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('menumanager_menu')->getId()) {
            return Mage::helper('cms')->__("Edit Menu '%s'", $this->htmlEscape(Mage::registry('menumanager_menu')->getData('label')));
        }
        else {
            return Mage::helper('cms')->__('New Menu');
        }
    }

    /**
     * Get the form action url
     *
     * @return string
     */
    public function getFormActionUrl(){
        return $this->getUrl('*/*/save', array('id' => Mage::registry('menumanager_menu')->getId()));
    }
}
