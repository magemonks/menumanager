<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Adminhtml_Menu_Edit_Embed extends Mage_Core_Block_Template
{

    /**
     * Get's the menu identifier
     * @return mixed
     */
    public function getIdentifier(){
        return Mage::registry('menumanager_menu')->getData('identifier');
    }

    protected function _toHtml(){
        if (!Mage::registry('menumanager_menu')) {
            return '';
        }
        else return parent::_toHtml();
    }
}