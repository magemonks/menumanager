<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Adminhtml_Menu_Tree extends Mage_Adminhtml_Block_Template
{
    
    public function __construct() {
        parent::__construct();
        $this->setTemplate('magemonks/menumanager/tree.phtml');
    }

    /**
     * Get the menu's root item
     * @return Magemonks_Menumanager_Model_Item
     */
    protected function _getRootItem(){
        return $this->_getMenu()->getRootItem();
        
    }
    
    /**
     * Get the menu in the registry
     * @return Magemonks_Menumanager_Model_Menu
     */
    protected function _getMenu(){
        return Mage::registry('menumanager_menu');
    }
    
    /**
     * Recursive function to get items' json object
     * 
     * @param Magemonks_Menumanager_Model_Item $item
     * @return Object json 
     */
    protected function _getItemJson(Magemonks_Menumanager_Model_Item $item){
        $helper = Mage::helper('magemonks');

        $state = 'closed';
        if($item->hasChildren()){
            $state = 'open';
        }

        $url = Mage::getModel('adminhtml/url')->getUrl('adminhtml/menumanager_item/edit', array('id' => $item->getId()));

        $json = $helper->getJsonWrapper(array(
           'state' => $item->hasChildren(true) ? $state : "",
           'attr' => $helper->getJsonWrapper(array(
               'id' => "treenode_".$item->getId(),
               'rel' => $item->getData('type'),
               'class' => ($item->getData('is_active') == 1 ? '' : 'jstree-leaf-disabled') . ($item->get('width') ? ' column_width_'.$item->get('width') : '')
           )),
           'data' => $helper->getJsonWrapper(array(
               'title' => $item->getData('label'),
               'attr' => $helper->getJsonWrapper(array(
                   'href' => $url
                ))
            )),
           'metadata' => $helper->getJsonWrapper(array(
               'id' => $item->getId(),
               'column_width' => $item->get('width')
            ))
        ));
        
        if($item->hasChildren(true)){
            $json->children = array();
            foreach($item->getChildren() as $child){
                $json->children[] = $this->_getItemJson($child);
            }
        }
        
        return $json;
    }
    
    
    /**
     * Get the tree's json object
     * @return String jsonencoded 
     */
    public function getTreeJson(){
        $json = $this->_getItemJson($this->_getRootItem());
        return Mage::helper('core')->jsonEncode($json);
    }
    
    
    
    public function getTypesJson(){
        $helper = Mage::helper('magemonks');
        
        $json = $helper->getJsonWrapper(array());
        
        $modelPaths = Mage::helper('menumanager/loader')->getItemTypes();
        foreach($modelPaths as $modelPath){
            $item = Mage::getModel($modelPath);
            $json->{$item->getData('type')} = $item->getAdminTypesConfiguration();
        }
        return Mage::helper('core')->jsonEncode($json);
    }
    
    /**
     * Get the url used for ajax
     * @return String url 
     */
    public function getAjaxUrl(){
        return Mage::getModel('adminhtml/url')->getUrl('adminhtml/menumanager_item/ajax');
    }
    
    /**
     *
     * @return Magemonks_Menumanager_Model_Item
     */
    public function getRootNodeId(){
        return $this->_getRootItem()->getId();
    }  
    
    /**
     * Get the id of the menu
     * @return Int
     */
    public function getMenuId(){
        return $this->_getMenu()->getId();
    }
    
    /**
     * Get the sessions formkey
     * @return String
     */
    public function getFormKey(){
        return Mage::getSingleton('core/session')->getFormKey();
    }
}