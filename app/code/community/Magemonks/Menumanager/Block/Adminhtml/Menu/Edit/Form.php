<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Adminhtml_Menu_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('menu_form');
        $this->setTitle(Mage::helper('menumanager')->__('Menu Information'));
    }

    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('menumanager_menu');

        $form = new Magemonks_Data_Form(
            array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post')
        );

        $form->setHtmlIdPrefix('menu_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('menumanager')->__('General'), 'class' => 'fieldset-wide'));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('label', 'text', array(
            'name'      => 'label',
            'label'     => Mage::helper('menumanager')->__('Menu Label'),
            'title'     => Mage::helper('menumanager')->__('Menu Label'),
            'required'  => true,
            'note'      => Mage::helper('menumanager')->__('The label of the menu. For internal reference only. Not displayed on the front-end'),
        ));

        $typeField = array(
            'name'      => 'type',
            'label'     => Mage::helper('menumanager')->__('Menu Type'),
            'title'     => Mage::helper('menumanager')->__('Menu Type'),
            'required'  => true,
            'options'   => array(
                'horizontal' => Mage::helper('menumanager')->__('Horizontal'),
                'vertical'   => Mage::helper('menumanager')->__('Vertical'),
            ),
            'note'      => Mage::helper('menumanager')->__('The menu type'),
            'class'     => 'validate-select',
            'value'     => 'horizontal'
        );
        if ($model->getMenuId()) {
            $typeField['disabled'] = true;
        }
        $fieldset->addField('type', 'select', $typeField);


        $fieldset->addField('identifier', 'text', array(
            'name'      => 'identifier',
            'label'     => Mage::helper('menumanager')->__('Identifier'),
            'title'     => Mage::helper('menumanager')->__('Identifier'),
            'required'  => true,
            'class'     => 'validate-xml-identifier',
            'note'      => Mage::helper('menumanager')->__('Used to load the menu'),
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('menumanager')->__('Store View'),
                'title'     => Mage::helper('menumanager')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                'note'      => Mage::helper('menumanager')->__('Select the stores where this menu is visible'),
                'class'     => 'validate-select',
            ));
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('menumanager')->__('Status'),
            'title'     => Mage::helper('menumanager')->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('menumanager')->__('Enabled'),
                '0' => Mage::helper('menumanager')->__('Disabled'),
            ),
        ));
        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }


        $fieldset = $form->addFieldset('htmltags_fieldset', array('legend'=>Mage::helper('menumanager')->__('HTML Tags'), 'class' => 'fieldset-wide'));


        $fieldset->addField('menu_tag', 'select', array(
            'name'      => 'menu_tag',
            'label'     => Mage::helper('menumanager')->__('Menu tag'),
            'title'     => Mage::helper('menumanager')->__('Menu tag'),
            'required'  => true,
            'note'      => Mage::helper('menumanager')->__('Set the HTML tag of the Menu.'),
            'values'    => Mage::getModel('magemonks/source_htmltag')->toOptionArray('ul'),
            'class'     => 'validate-select',
        ));

        $fieldset->addField('menu_tag_id', 'text', array(
            'name'      => 'menu_tag_id',
            'label'     => Mage::helper('menumanager')->__('Menu ID'),
            'title'     => Mage::helper('menumanager')->__('Menu ID'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Set the HTML ID of the menu.'),
            'class'     => 'validate-xml-identifier'
        ));

        $fieldset->addField('menu_tag_class', 'text', array(
            'name'      => 'menu_tag_class',
            'label'     => Mage::helper('menumanager')->__('Menu CSS class(es)'),
            'title'     => Mage::helper('menumanager')->__('Menu CSS class(es)'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Set the (extra) CSS class(es) for the menu tag.'),
        ));

        $fieldset->addField('submenu_tag', 'select', array(
            'name'      => 'submenu_tag',
            'label'     => Mage::helper('menumanager')->__('Submenu tag'),
            'title'     => Mage::helper('menumanager')->__('Submenu tag'),
            'required'  => true,
            'note'      => Mage::helper('menumanager')->__('Set the HTML tag for the submenus.'),
            'values'    => Mage::getModel('magemonks/source_htmltag')->toOptionArray('ul'),
            'class'     => 'validate-select',
        ));

        $fieldset->addField('submenu_tag_class', 'text', array(
            'name'      => 'submenu_tag_class',
            'label'     => Mage::helper('menumanager')->__('Submenu CSS class(es)'),
            'title'     => Mage::helper('menumanager')->__('Submenu CSS class(es)'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Set the (extra) CSS class(es) for the submenu tags.'),
        ));

        $fieldset->addField('item_tag', 'select', array(
            'name'      => 'item_tag',
            'label'     => Mage::helper('menumanager')->__('Item tag'),
            'title'     => Mage::helper('menumanager')->__('Item tag'),
            'required'  => true,
            'note'      => Mage::helper('menumanager')->__('Set the HTML tag for the menu items.'),
            'values'    => Mage::getModel('magemonks/source_htmltag')->toOptionArray('li'),
            'class'     => 'validate-select',
        ));

        $fieldset->addField('item_tag_class', 'text', array(
            'name'      => 'item_tag_class',
            'label'     => Mage::helper('menumanager')->__('Item CSS class(es)'),
            'title'     => Mage::helper('menumanager')->__('Item CSS class(es)'),
            'note'      => Mage::helper('menumanager')->__('Set the (extra) CSS class(es) for the menu item.'),
            'required'  => false
        ));

        $fieldset->addField('item_active_class', 'text', array(
            'name'      => 'item_active_class',
            'label'     => Mage::helper('menumanager')->__('Active item CSS class'),
            'title'     => Mage::helper('menumanager')->__('Active item CSS class'),
            'note'      => Mage::helper('menumanager')->__('Set active item CSS class. Recommended: "active"'),
            'required'  => true,
            'class'     => 'validate-xml-identifier',
            'value'     =>  'active'
        ));

        $fieldset->addField('item_active_parent_class', 'text', array(
            'name'      => 'item_active_parent_class',
            'label'     => Mage::helper('menumanager')->__('Active parent CSS class'),
            'title'     => Mage::helper('menumanager')->__('Active parent CSS class'),
            'note'      => Mage::helper('menumanager')->__('Set active item CSS class. Recommended: "active-parent"'),
            'required'  => false,
            'class'     => 'validate-xml-identifier'
        ));



        $fieldset = $form->addFieldset('megamenu_fieldset', array('legend'=>Mage::helper('menumanager')->__('Mega menus'), 'class' => 'fieldset-wide'));

        $fieldset->addField('megamenu_columns_per_row', 'text', array(
            'name'      => 'megamenu_columns_per_row',
            'label'     => Mage::helper('menumanager')->__('Columns per row'),
            'title'     => Mage::helper('menumanager')->__('Columns per row'),
            'note'      => Mage::helper('menumanager')->__('Set the <strong>maximum</strong> number of columns that fit in a row'),
            'required'  => true,
            'class'     => 'validate-number validate-greater-than-zero',
            'value'     => 6
        ));


        $fieldset->addField('megamenu_tag', 'select', array(
            'name'      => 'megamenu_tag',
            'label'     => Mage::helper('menumanager')->__('Mega menu tag'),
            'title'     => Mage::helper('menumanager')->__('Mega menu tag'),
            'note'      => Mage::helper('menumanager')->__('Set the HTML tag for the Mega menus.'),
            'required'  => true,
            'values'    => Mage::getModel('magemonks/source_htmltag')->toOptionArray('div'),
            'class'     => 'validate-select',
        ));

        $fieldset->addField('megamenu_tag_class', 'text', array(
            'name'      => 'megamenu_tag_class',
            'label'     => Mage::helper('menumanager')->__('Mega menu CSS class(es)'),
            'title'     => Mage::helper('menumanager')->__('Mega menu CSS class(es)'),
            'note'      => Mage::helper('menumanager')->__('Set custom (extra) CSS class(es) for the Mega menu'),
        ));

        $fieldset->addField('megamenu_item_tag', 'select', array(
            'name'      => 'megamenu_item_tag',
            'label'     => Mage::helper('menumanager')->__('Mega menu item tag'),
            'title'     => Mage::helper('menumanager')->__('Mega menu item tag'),
            'note'      => Mage::helper('menumanager')->__('Set the HTML tag for the Mega menu items.'),
            'required'  => true,
            'values'    => Mage::getModel('magemonks/source_htmltag')->toOptionArray('div'),
            'class'     => 'validate-select',
        ));

        $fieldset->addField('megamenu_item_tag_class', 'text', array(
            'name'      => 'megamenu_item_tag_class',
            'label'     => Mage::helper('menumanager')->__('Mega menu item CSS class(es)'),
            'title'     => Mage::helper('menumanager')->__('Mega menu item CSS class(es)'),
            'note'      => Mage::helper('menumanager')->__('Set custom (extra) CSS class(es) for the Mega menu item'),
        ));

        /* Advanced fields */

        $fieldset = $form->addFieldset('advanced_fieldset', array('legend'=>Mage::helper('menumanager')->__('Advanced'), 'class' => 'fieldset-wide'));

        $fieldset->addField('custom_template', 'text', array(
            'name'      => 'custom_template',
            'label'     => Mage::helper('menumanager')->__('Custom template'),
            'title'     => Mage::helper('menumanager')->__('Custom template'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Allows to set a custom rendering template (example: foo/bar.phtml)'),
        ));

        $fieldset->addField('max_child_depth', 'text', array(
            'name'      => 'max_child_depth',
            'label'     => Mage::helper('menumanager')->__('Max child depth'),
            'title'     => Mage::helper('menumanager')->__('Max child depth'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Set the maximum depth of children.'),
        ));

        $fieldset->addField('html_before', 'textarea', array(
            'name'      => 'html_before',
            'label'     => Mage::helper('menumanager')->__('HTML Before'),
            'title'     => Mage::helper('menumanager')->__('HTML Before'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Add custom HTML before the menu'),
        ));

        $fieldset->addField('html_after', 'textarea', array(
            'name'      => 'html_after',
            'label'     => Mage::helper('menumanager')->__('HTML After'),
            'title'     => Mage::helper('menumanager')->__('HTML After'),
            'required'  => false,
            'note'      => Mage::helper('menumanager')->__('Add custom HTML after the menu'),
        ));

        $fieldset->addField('disallowed_item_types', 'multiselect', array(
            'name'      => 'disallowed_item_types',
            'label'     => Mage::helper('menumanager')->__('Disallowed item types'),
            'title'     => Mage::helper('menumanager')->__('Disallowed item types'),
            'note'      => Mage::helper('menumanager')->__('Select the disallowed item types (hold ctrl (cmd on a Mac) to select more than one). This will prevent the user from creating new items of those types.'),
            'required'  => false,
            'values'   => Mage::helper('menumanager/loader')->getItemTypesOptions()
        ));

        $form->setValues($model->getData(), true);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
