<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Adminhtml_Item_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Additional buttons on category page
     *
     * @var array
     */
    protected $_additionalButtons = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('magemonks/menumanager/item/form.phtml');
        $this->setShowGlobalIcon(false);
    }

    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        $this->setChild('back_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'id'        => "backToMenu",
                    'onclick'   => "setLocation('".Mage::getModel('adminhtml/url')->getUrl('adminhtml/menumanager_menu')."')",
                    'label'     => Mage::helper('menumanager')->__('Back'),
                    'class' => 'back'
            ))
        );

        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'id'        => "menumanagerItemSave",
                    'label'     => Mage::helper('menumanager')->__('Save'),
                    'class' => 'save'
                ))
        );

        $this->setChild('save_enable_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'id'        => "menumanagerItemSaveEnable",
                'label'     => Mage::helper('menumanager')->__('Save & Enable'),
                'class' => 'save'
            ))
        );

        $this->setChild('reset_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'id'        => "menumanagerItemReset",
                    'label'     => Mage::helper('catalog')->__('Reset'),
                    'class' => 'reset'
                ))
        );
        
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        
        parent::_prepareLayout();
    }

    /**
     * @return string
     */
    public function getBackButtonHtml()
    {
        return $this->getChildHtml('back_button');
    }

    /**
     * @return string
     */
    public function getSaveButtonHtml()
    {
        return $this->getChildHtml('save_button');
    }

    /**
     * @return string
     */
    public function getSaveEnableButtonHtml()
    {
        return $this->getChildHtml('save_enable_button');
    }

    /**
     * @return string
     */
    public function getResetButtonHtml()
    {
        return $this->getChildHtml('reset_button');
    }


    /**
     * @return string
     */
    public function getHeaderText(){
        $item = $this->getMenuItem();
        return Mage::helper('menumanager')->__("%s <small>(%s ID: %s)</small>", $item->getData('label'), $item->getCreationLabel(), $item->getID());
    }
    
    /**
     * Retrieve additional buttons html
     *
     * @return string
     */
    public function getAdditionalButtonsHtml()
    {
        $html = '';
        foreach ($this->_additionalButtons as $childName) {
            $html .= $this->getChildHtml($childName);
        }
        return $html;
    }
    
    /**
     * Add additional button
     *
     * @param string $alias
     * @param array $config
     * @return Mage_Adminhtml_Block_Catalog_Category_Edit_Form
     */
    public function addAdditionalButton($alias, $config)
    {
        if (isset($config['name'])) {
            $config['element_name'] = $config['name'];
        }
        $this->setChild($alias . '_button',
                        $this->getLayout()->createBlock('adminhtml/widget_button')->addData($config));
        $this->_additionalButtons[$alias] = $alias . '_button';
        return $this;
    }

    /**
     * Remove additional button
     *
     * @param string $alias
     * @return Mage_Adminhtml_Block_Catalog_Category_Edit_Form
     */
    public function removeAdditionalButton($alias)
    {
        if (isset($this->_additionalButtons[$alias])) {
            $this->unsetChild($this->_additionalButtons[$alias]);
            unset($this->_additionalButtons[$alias]);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        return Mage::app()->getRequest()->isXmlHttpRequest() || Mage::app()->getRequest()->getParam('isAjax');
    }

    /**
     * @return mixed
     */
    public function getMenu(){
        return Mage::registry('menumanager_menu');
    }

    /**
     * @return mixed
     */
    public function getMenuItem(){
        return Mage::registry('menumanager_item');
    }

    /**
     * @return mixed
     */
    public function getItemId(){
        return $this->getMenuItem()->getId();
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $item = $this->getMenuItem();
        
        $form = $item->getAdminForm($this);
        $form->setData(array(
            'id' => 'menumanagerItemEditForm',
            'action' => Mage::getModel('adminhtml/url')->getUrl('adminhtml/menumanager_item/save', array('id' =>$item->getId())),
            'method' => 'post',
            'html_id_prefix' => 'item_'
        ));

        $form->setValues($item->get(), true);

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
