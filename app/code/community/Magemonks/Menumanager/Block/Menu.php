<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Menu extends Mage_Core_Block_Template {

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('magemonks/menumanager/menu.phtml');

        $this->addData(array(
            'cache_lifetime'=> false,
            'cache_tags'    => array(
                Magemonks_Menumanager_Model_Menu::CACHE_TAG,
                Mage_Core_Model_Store::CACHE_TAG,
                Mage_Core_Model_Store_Group::CACHE_TAG,
                Mage_Catalog_Model_Category::CACHE_TAG,
                Mage_Catalog_Model_Product::CACHE_TAG,
                Mage_Catalog_Model_Product_Url::CACHE_TAG,
                Mage_Catalog_Model_Product_Type_Price::CACHE_TAG,
                Mage_Eav_Model_Entity_Attribute::CACHE_TAG,
                Mage_Cms_Model_Block::CACHE_TAG,
                Mage_Cms_Model_Page::CACHE_TAG,
            )
        ));
    }

    /**
     * Get cache key informative items
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'MENUMANAGER_MENU',
            Mage::app()->getStore()->getId(),
            (int)Mage::app()->getStore()->isCurrentlySecure(),
            md5(Mage::getModel('core/url')->sessionUrlVar(Mage::helper('core/url')->getCurrentUrl())),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            $this->getMenu()->getData('identifier'),
            $this->getTemplate(),
            (string) $this->getData('max_child_depth'),
            (string) $this->getData('root_item_id')
        );
    }

    /**
     * Sets the menu identifier to load
     *
     * @param $identifier
     * @return Magemonks_Menumanager_Block_Menu
     */
    public function setIdentifer($identifier){
        $this->setData('identifier', $identifier);
        return $this;
    }

    /**
     * Sets the menu idTag (overrides)
     *
     * @param $idTag
     * @return Magemonks_Menumanager_Block_Menu
     */
    public function setIdTag($idTag){
        $this->setData('id_tag', $idTag);
        return $this;
    }

    /**
     * Set's the max depth of the children
     *
     * @param $maxChildDepth
     * @return Magemonks_Menumanager_Block_Menu
     */
    public function setMaxChildDepth($maxChildDepth){
        $this->setData('max_child_depth', (int) $maxChildDepth);
        return $this;
    }

    /**
     * Set's a custom root item id (id of a menu item)
     *
     * @param $rootId
     * @return Magemonks_Menumanager_Block_Menu
     */
    public function setRootItemId($rootId){
        $this->setData('root_item_id', $rootId);
        return $this;
    }

    /* Functions to fool Magento */
    public function addLink()
    {
        return $this;
    }
    public function addLinkBlock($blockName)
    {
        return $this;
    }
    public function removeLinkBlock($blockName)
    {
        return $this;
    }
    public function removeLinkByUrl($url)
    {
        return $this;
    }

    /**
     * Retrieve Menu instance
     *
     * @return Magemonks_Menumanager_Model_Menu
     */
    public function getMenu()
    {
        if (!$this->hasData('menu')) {
            $identifier = $this->getData('identifier', null);
            if ($identifier) {
                $menu = Mage::getModel('menumanager/menu')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($identifier);
                if($menu->getData('is_active') == true){
                    $maxChildDepth = $this->getData('max_child_depth');
                    if(!empty($maxChildDepth)){
                        $menu->setData('max_child_depth', $maxChildDepth);
                    }
                    $rootItemId = $this->getData('root_item_id');
                    if(!empty($rootItemId)){
                        $menu->setData('root_item_id', $rootItemId);
                    }

                    $this->setData('menu', $menu);
                }
            }
        }
        return $this->getData('menu');
    }


    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     */
    public function _beforeToHtml()
    {
        parent::_beforeToHtml();
        if($menu = $this->getMenu()){
            $custom_template = $menu->getData('custom_template', null);
            if(!empty($custom_template)){
                $this->setTemplate($menu->getData('custom_template'));
            }
        }
        return $this;
    }

    /**
     * Get the menu wrapper opening tag
     *
     * @return string
     */
    public function getMenuWrapperOpen()
    {
        $menu = $this->getData('menu');
        $idValue     = $this->getData('id_tag') ? $this->getData('id_tag') : $menu->getData('menu_tag_id');
        $id          = $idValue ? sprintf(' id="%s"', $idValue) : '';

        $classes     = 'menumanager-menu '.
            'menumanager-menu-type-'.$menu->getData('type').' '.
            'menumanager-menu-identifier-'.$menu->getData('identifier').' '.
            ($menu->getData('menu_tag_class') ? $menu->getData('menu_tag_class').' ' : '');
        $class       = sprintf(' class="%s"', $classes);

        $tag         = $menu->getData('menu_tag');
        return sprintf('<%1$s%2$s%3$s>', $tag, $id, $class);
    }

    /**
     * Get the menu wrapper closing tag
     *
     *
     * @return string
     */
    public function getMenuWrapperClose()
    {
        $menu = $this->getData('menu');
        $tag         = $menu->getData('menu_tag');
        return sprintf('</%1$s>', $tag);
    }

    /**
     * Get the HTML for the menu contents
     *
     *
     * @return string
     */
    public function getHtml()
    {
        $menu = $this->getMenu();
        if(!$menu) return '';

        $rootItem = $menu->getRootItem();
        if(!$rootItem) return '';

        //allow to override the root item in the block
        $rootItemId = $this->getData('root_item_id');
        if(false === empty($rootItemId)){
            //show menu from given root item

            /* @var $rootItem Magemonks_Menumanager_Model_Item_Root */
            $rootItem = $menu->getItemById($rootItemId);
            if(!$rootItem) return '';

            /* @var $blocks Magemonks_Menumanager_Block_Item[] */
            $blocks = $rootItem->getBlocks();
            if(!is_array($blocks)) return '';

            $html = '';
            foreach($blocks as $block){
                $block->setData('level', -1);
                $html .= $block->getItemChildHtml();
            }
        }
        else{
            //regular root item
            /* @var $blocks Magemonks_Menumanager_Block_Item[] */
            $blocks = $rootItem->getBlocks();
            if(!is_array($blocks)) return '';
            $html = $blocks[0]->toHtml();
        }
        return $html;
    }
}