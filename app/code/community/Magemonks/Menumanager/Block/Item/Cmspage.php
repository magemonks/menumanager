<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Cmspage extends Magemonks_Menumanager_Block_Item {

    /**
     * Set CMS page
     * @return null|Mage_Cms_Model_Page
     */
    public function getCmsPage()
    {
        $cms_page = $this->getData('cms_page');
        if(is_null($cms_page)){
            $identifier = $this->getItemData('cms_page_identifier', null);
            if(!$identifier){
                return null;
            }

            $cms_page = Mage::getModel('cms/page')->load($identifier, 'identifier');
            if(!$cms_page->getIsActive()){
                return null;
            }

            $this->setData('cms_page', $cms_page);
        }
        return $this->getData('cms_page');
    }

    /**
     * Set the CMS page
     *
     * @param Mage_Cms_Model_Page $page
     * @return Varien_Object
     */
    public function setCmsPage(Mage_Cms_Model_Page $page)
    {
        return $this->setData('cms_page', $page);
    }

    /**
     * Get the anchor link
     *
     * @return null|string
     */
    public function getAnchorLink()
    {
        $cms_page = $this->getCmsPage();
        if($cms_page){
            return Mage::Helper('cms/page')->getPageUrl($cms_page->getId());
        }
        return null;
    }

    /**
     * Get the title
     *
     * @return null|string
     */
    public function getTitle()
    {
        $title = $this->getItemData('title', null);
        if(!empty($title)){
            return Mage::helper('menumanager')->__($title);
        }

        $cms_page = $this->getCmsPage();
        if($cms_page){
            return $cms_page->getTitle();
        }
        return null;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $title = $this->getTitle();
        $anchor_link = $this->getAnchorLink();
        if(!empty($title) && !empty($anchor_link)){
            return parent::canDisplay();
        }
        return false;
    }

    /**
     * Is the current menu item active
     *
     * @return bool
     */
    protected function _isLinkActive()
    {
        $request = Mage::app()->getRequest();

        return $request->getModuleName() === 'cms' && $request->getControllerName() === 'page' && $request->getActionName() === 'view' && $this->getCmsPage()->getId() === $request->getParam('page_id');
    }
}