<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Category extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the anchor link
     *
     * @return null|string
     */
    public function getAnchorLink()
    {
        $category = $this->getCategory();
        if($category){
            return $this->getCategoryUrl($category);
        }
        return null;
    }

    /**
     * Get the title
     *
     * @return null|string
     */
    public function getTitle()
    {
        $title = $this->getItemData('title', null);
        if(!empty($title)){
            return Mage::helper('menumanager')->__($title);
        }

        $category = $this->getCategory();
        if($category){
            return $this->escapeHtml($category->getName());
        }
        return null;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $title = $this->getTitle();
        $anchorlink = $this->getAnchorLink();
        if(!empty($title) && !empty($anchorlink)){
            return parent::canDisplay();
        }
        return false;
    }

    /**
     * Get the category
     *
     * @return mixed|null
     */
    public function getCategory()
    {
        $category = $this->getData('category');
        if(is_null($category)){
            $id = $this->getItemData('category_id', null);
            if(!$id) return null;
            $category =  Mage::getModel("catalog/category")->load($id);

            if (!$category->getId()) return null;
            if (!$category->getIsActive()) return null;
            if (!$category->isInRootCategoryList()) return null;

            $this->setCategory($category);

        }
        return $this->getData('category');
    }

    /**
     * Set the category
     *
     * @param $category
     * @return Varien_Object
     */
    public function setCategory($category)
    {
        return $this->setData('category', $category);
    }



    /**
     * Get the category that currently is being visited by the user
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCurrentCategory()
    {
        if (Mage::getSingleton('catalog/layer')) {
            return Mage::getSingleton('catalog/layer')->getCurrentCategory();
        }
        return false;
    }

    /**
     * Checkin activity of category
     *
     * @param   Varien_Object $category
     * @return  bool
     */
    public function isCategoryActive($category)
    {
        return in_array($category->getId(), $this->getCurrentCategory()->getPathIds());
    }

    /**
     * Get url for category data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getCategoryUrl($category)
    {
        if ($category instanceof Mage_Catalog_Model_Category) {
            $url = $category->getUrl();
        } else {
            $url = $this->_getCategoryInstance()
                ->setData($category->getData())
                ->getUrl();
        }

        return $url;
    }

    /**
     * Get an instance of a category
     *
     * @return Mage_Catalog_Model_Category
     */
    protected function _getCategoryInstance()
    {
        if (is_null($this->_categoryInstance)) {
            $this->_categoryInstance = Mage::getModel('catalog/category');
        }
        return $this->_categoryInstance;
    }

    /**
     * Is the current menu item active
     *
     * @return bool
     */
    protected function _isLinkActive()
    {
        $category = $this->getCategory();
        $currentCategory = $this->getCurrentCategory();

        if(!$category instanceof Mage_Catalog_Model_Category || ! $currentCategory instanceof Mage_Catalog_Model_Category) {
            return false;
        }
        return $category->getEntityId() === $currentCategory->getEntityId();
    }
}