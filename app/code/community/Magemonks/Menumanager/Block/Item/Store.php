<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Store extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the store
     *
     * @return null|array
     */
    public function getStoreConfig()
    {
        $storeConfig = $this->getData('store_config');
        if(is_null($storeConfig)){

            $store_string = $this->getItemData('store_string', null);
            if(empty($store_string)) return null;

            $store_string = explode('_', $store_string);
            if(!is_array($store_string) || count($store_string) != 2) return null;
            $type = $store_string[0];
            $id = $store_string[1];
            if(!is_string($type)) return null;
            if(!is_numeric($id)) return null;

            if($type == 'website'){
                $website = Mage::getModel('core/website')->load($id);
                $title = $website->getName();
                if($website instanceof Mage_Core_Model_Website){
                    $group = $website->getDefaultGroup();
                }
                else{
                    return null;
                }
            }
            if($type == 'group' || isset($group)){
                if(!isset($group)){
                    $group = Mage::getModel('core/store_group')->load($id);
                    if(!isset($title)) $title = $group->getName();
                }
                if($group instanceof Mage_Core_Model_Store_Group){
                    $store = $group->getDefaultStore();
                }
                else{
                    return null;
                }
            }
            if($type == 'store' || isset($store)){
                if(!isset($store)){
                    $store = Mage::getModel('core/store')->load($id);
                    if(!isset($title)) $title = $store->getName();
                }
                if($store instanceof Mage_Core_Model_Store){
                    $storeConfig = array(
                        'title' => $title,
                        'url' => $store->getBaseUrl(),
                        'store' => $store
                    );
                    $this->setData('storeConfig', $storeConfig);
                }
                else{
                    return null;
                }
            }
            else{
                return null;
            }
        }
        return $this->getData('storeConfig');
    }

    /**
     * Get the link href value
     *
     * @return null|string
     */
    public function getAnchorLink()
    {
        $storeConfig = $this->getStoreConfig();
        if(is_array($storeConfig) && isset($storeConfig['url'])){
            return $storeConfig['url'];
        }
        return null;
    }

    /**
     * Get the title to be shown in the menu
     *
     * @return null|string
     */
    public function getTitle()
    {
        $title = $this->getItemData('title', null);
        if(!empty($title)){
            return Mage::helper('menumanager')->__($title);
        }

        $storeConfig = $this->getStoreConfig();
        if(is_array($storeConfig) && isset($storeConfig['title'])){
            return $storeConfig['title'];
        }
        return null;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $title = $this->getTitle();
        $anchorlink = $this->getAnchorLink();
        if(!empty($title) && !empty($anchorlink)){
            return parent::canDisplay();
        }
        return false;
    }


    /**
     * Is the current menu item active
     *
     * @return bool
     */
    protected function _isLinkActive()
    {
        $storeConfig = $this->getStoreConfig();
        if(!is_array($storeConfig)) { return false; }

        $store = $storeConfig['store'];
        if(!$store instanceof Mage_Core_Model_Store) { return false; }

        if($store->getId() ===  Mage::app()->getStore()->getId()){
            return true;
        }

        return false;
    }
}