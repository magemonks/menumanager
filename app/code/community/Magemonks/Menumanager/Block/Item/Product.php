<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Product extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the product
     *
     * @return null|Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        $product = $this->getData('product');
        if(is_null($product)){
            $id = $this->getProductId();
            if(!$id) return null;

            $helper = Mage::helper('catalog/product');
            $product = $helper->getProduct($id, Mage::app()->getStore()->getId());
            if(!$product->getId()) return null;
            if(!$product->isVisibleInCatalog() || !$product->isVisibleInSiteVisibility()) return null;
            if(!in_array(Mage::app()->getStore()->getWebsiteId(), $product->getWebsiteIds())) return null;
            if($this->getItemData('out_of_stock') === 'hide' && !$product->getStockItem()->getIsInStock()) return null;

            $this->setData('product', $product);
        }
        return $this->getData('product');
    }

    /**
     * Get the product id
     *
     * @return null|string
     */
    public function getProductId()
    {
        $productId = $this->getData('product_id');
        if(is_null($productId)){
            $this->setData('product_id', $this->getItemData('product_id'));
        }
        return $this->getData('product_id');
    }

    /**
     * Get the link href value
     *
     * @return null|string
     */
    public function getAnchorLink()
    {
        $product = $this->getProduct();
        if($product){
            return $product->getUrlModel()->getUrl($product, array('_ignore_category' => true));
        }
        return null;
    }


    /**
     * Get the title to be shown in the menu
     *
     * @return null|string
     */
    public function getTitle()
    {
        $title = $this->getItemData('title', null);
        if(!empty($title)){
            return $this->stripTags(Mage::helper('menumanager')->__($title), null, true);
        }
        $product = $this->getProduct();
        if($product){
            return $this->stripTags($product->getName(), null, true);
        }
        return null;
    }

    /**
     * Get the product that currently is being visited by the user
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getCurrentProduct()
    {
        return Mage::registry('current_product');
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $product = $this->getProduct();
        if(is_null($product)){
            return false;
        }

        $title = $this->getTitle();
        $anchorlink = $this->getAnchorLink();
        if(!empty($title) && !empty($anchorlink)){
            return parent::canDisplay();
        }
        return false;
    }

    /**
     * Is the current menu item active
     *
     * @return bool
     */
    protected function _isLinkActive()
    {
        $product = $this->getProduct();
        $currentProduct = $this->getCurrentProduct();

        if(!$product instanceof Mage_Catalog_Model_Product || ! $currentProduct instanceof Mage_Catalog_Model_Product) {
            return false;
        }
        return $product->getEntityId() === $currentProduct->getEntityId();
    }
}