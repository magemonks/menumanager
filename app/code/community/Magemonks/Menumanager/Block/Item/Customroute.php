<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Customroute extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the link href value
     *
     * @return null|string
     */
    public function getAnchorLink()
    {
        $route = $this->getItemData('custom_route', null);
        if(empty($route)) return null;

        $routeParams = array();
        $params =  $this->getItemData('params', null);
        if(!empty($params)){
            $params = explode("\n", $params);
            if(is_array($params)){
                foreach($params as $param){
                    $param = explode("=", $param);
                    if(is_array($param) && count($param) == 2){
                        $routeParams[trim($param[0])] = trim($param[1]);
                    }
                }
            }
        }

        $url = $this->getUrl($route, $routeParams);
        if(empty($url)) return null;
        return $url;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $title = $this->getTitle();
        $anchorlink = $this->getAnchorLink();
        if(!empty($title) && !empty($anchorlink)){
            return parent::canDisplay();
        }
        return false;
    }
}