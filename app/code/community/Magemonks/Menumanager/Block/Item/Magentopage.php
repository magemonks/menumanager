<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Magentopage extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the page config
     *
     * @return null|array
     */
    public function getPageConfig()
    {
        $page_config = $this->getData('page_config');
        if(is_null($page_config)){
            $page_type = $this->getItemData('page_type', null);
            if(empty($page_type)) return null;

            $page_config = Mage::helper("magemonks/magentopage")->getPage($page_type);

            if(empty($page_config)) return null;

            $this->setData('page_config', $page_config);
        }
        return $this->getData('page_config');
    }

    /**
     * Sets the page config
     *
     * @param array $page_config
     * @return Varien_Object
     */
    public function setPageConfig(array $page_config)
    {
        return $this->setData('page_config', $page_config);
    }

    /**
     * Get the link href value
     *
     * @return null|string
     */
    public function getAnchorLink()
    {
        $page_config = $this->getPageConfig();
        if(is_array($page_config) && isset($page_config['url'])){
            return $page_config['url'];
        }
        return null;
    }

    /**
     * Get the title to be shown in the menu
     *
     * @return null|string
     */
    public function getTitle()
    {
        $title = $this->getItemData('title', null);
        if(!empty($title)){
            return Mage::helper('menumanager')->__($title);
        }

        $page_config = $this->getPageConfig();
        if(is_array($page_config) && isset($page_config['title'])){
            return $page_config['title'];
        }
        return null;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $config = $this->getPageConfig();

        if(is_array($config)){
            if(isset($config['display_when_logged_in']) && $config['display_when_logged_in'] == 0 && $this->helper('customer')->isLoggedIn()){
                return false;
            }
            if(isset($config['display_when_logged_out']) && $config['display_when_logged_out'] == 0 && !$this->helper('customer')->isLoggedIn()){
                return false;
            }
        }

        $title = $this->getTitle();
        $anchorlink = $this->getAnchorLink();
        if(!empty($title) && !empty($anchorlink)){
            return parent::canDisplay();
        }
        return false;
    }


}