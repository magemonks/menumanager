<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Home extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the link href value
     *
     * @return string
     */
    public function getAnchorLink()
    {
        return Mage::helper('core/url')->getHomeUrl();
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $title = $this->getTitle();
        $anchorlink = $this->getAnchorLink();
        if(!empty($title) && !empty($anchorlink)){
            return parent::canDisplay();
        }
        return false;
    }
}