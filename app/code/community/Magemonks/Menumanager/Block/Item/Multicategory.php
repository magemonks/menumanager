<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Multicategory extends Magemonks_Menumanager_Block_Item_Category {

    /**
     * Get the children of a multicategory.
     *
     * Be very careful with this function as it set's the children to be itself. In getChildBlocks it passes it's parent block
     * as a parameter. Based on the category of the parent, getChildBlocks will stop returning childBlocks (which in this case are
     * blocks for subcategories).
     *
     * @return array
     */
    public function getItemChildren()
    {
        return array($this->getItem());
    }
}