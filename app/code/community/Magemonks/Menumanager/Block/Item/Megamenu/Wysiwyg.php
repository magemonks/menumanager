<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Wysiwyg extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the content
     *
     * @return null|string
     */
    public function getContent()
    {
        $content = $this->getItemData('content');
        $remove_outer_p_tags = $this->getItemData('remove_outer_p_tags');

        if(empty($content)) return null;
        if($remove_outer_p_tags == true){
            if(strpos($content, '<p>') === 0){
                $content = substr($content, 3, strlen($content) - 7);
            }
        }
        return $content;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $content = $this->getContent();
        if(!empty($content)){
            return parent::canDisplay();
        }
        return false;
    }
}