<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Menu extends Magemonks_Menumanager_Block_Item {


    public function getChildBlocks(){
        $childBlocks = $this->getData('child_blocks');
        if(is_null($childBlocks)){
            $childBlocks = parent::getChildBlocks();
            if($this->getItemData('columnize') == 'enabled'){
                $parent = $this->getParent();
                if(is_numeric($width = $parent->getItemData('width'))){
                    $numItems = count($childBlocks);
                    $rowSize = $width;
                    $columnSize = ceil($numItems / $rowSize);
                    $itemsInLastRow = $numItems % $rowSize;

                    $newChildBlocks = array();
                    foreach($childBlocks as $childBlock){
                        $counter = $childBlock->getData('counter');
                        if($counter > $itemsInLastRow * $columnSize){
                            $x =  ceil((($counter - ($itemsInLastRow * $columnSize)) / ($columnSize - 1 ))) + $itemsInLastRow;
                            $y = $counter - ($itemsInLastRow * $columnSize) - (($x-1-$itemsInLastRow) *  ($columnSize-1));
                        }
                        else{
                            $x = ceil($counter / ($columnSize));
                            $y = $counter - (($x-1) *  $columnSize);
                        }
                        $newCounter = (($y-1) * $rowSize) + $x;

                        $childBlock->setData('counter', $newCounter);
                        $childBlock->setData('first', $newCounter == 1);
                        $childBlock->setData('last', $newCounter == $numItems);
                        $newChildBlocks[$newCounter] = $childBlock;
                    }
                    ksort($newChildBlocks);
                    $this->setData('child_blocks', $newChildBlocks);
                }
            }
        }
        return $this->getData('child_blocks');
    }



    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        return parent::canDisplay();
    }

}