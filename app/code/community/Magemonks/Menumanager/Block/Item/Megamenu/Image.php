<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Image extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the anhor link
     *
     * @return null|string
     */
    public function getAnchorLink()
    {
        $url = $this->getItemData('url');
        if(!empty($url)){
            return $url;
        }
        return null;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $image = $this->getImageTag();
        if(!empty($image)){
            return parent::canDisplay();
        }
        return false;
    }
}