<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Header extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the header wrapper opening tag
     * @return string
     */
    public function getHeaderWrapperOpen()
    {
        $tag =  $this->getItemData('header_tag');
        $class =  $this->getItemData('header_tag_class');
        $class = !empty($class) ? "class='".$class."'" : '';

        return sprintf('<%1$s %2$s>', $tag, $class);
    }

    /**
     * Get the header wrapper closing tag
     * @return string
     */
    public function getHeaderWrapperClose()
    {
        $tag =  $this->getItemData('header_tag');

        return sprintf('</%1$s>', $tag);
    }

    /**
     * Get the header text
     *
     * @return null|string
     */
    public function getHeaderText()
    {
        return $this->getItemData('header_text');
    }


    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $text =  $this->getItemData('header_text');
        $tag =  $this->getItemData('header_tag');

        if(!empty($text) && !empty($tag)){
            return parent::canDisplay();
        }
        return false;
    }
}