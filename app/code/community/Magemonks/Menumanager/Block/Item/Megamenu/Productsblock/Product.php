<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Productsblock_Product extends Mage_Catalog_Block_Product_Abstract
{
    protected $_menumanagerBlock = null;

    /**
     * Set the menumanager block
     *
     * @param Magemonks_Menumanager_Block_Item $block
     * @return Magemonks_Menumanager_Block_Item_Megamenu_Productsblock_Product
     */
    public function setMenumanagerBlock(Magemonks_Menumanager_Block_Item $block)
    {
        $this->_menumanagerBlock = $block;
        return $this;
    }

    /**
     * Get the menumanager block
     *
     * @return null|Magemonks_Menumanager_Block_Item
     */
    public function getMenumanagerBlock()
    {
        return $this->_menumanagerBlock;
    }
}