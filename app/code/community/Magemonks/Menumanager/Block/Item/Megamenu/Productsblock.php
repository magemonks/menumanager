<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Productsblock extends Magemonks_Menumanager_Block_Item_Multiproduct {

    /**
     * Get the product HTML
     *
     * @return string
     */
    public function getProductHtml()
    {
        $block = Mage::app()->getLayout()->createBlock('menumanager/item_megamenu_productsblock_product');
        $block->setTemplate('magemonks/menumanager/item/megamenu/productsblock/product.phtml');
        $block->setMenumanagerBlock($this);
        $block->setProduct($this->getProduct());

        return $block->toHtml();
    }
}