<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Column extends Magemonks_Menumanager_Block_Item {

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        return parent::canDisplay();
    }

    /**
     * Get the Item Tag Class
     *
     * @return string
     */
    public function getItemTagClass()
    {
        $class = parent::getItemTagClass().
                    ($this->getItemData('border_top') ? 'menu-item-border-top ' : '').
                    ($this->getItemData('border_right') ? 'menu-item-border-right ' : '').
                    ($this->getItemData('border_bottom') ? 'menu-item-border-bottom ' : '').
                    ($this->getItemData('border_left') ? 'menu-item-border-left ' : '');
        return $class;
    }
}