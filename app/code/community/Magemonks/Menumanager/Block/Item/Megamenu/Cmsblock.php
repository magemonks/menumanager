<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Cmsblock extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the CMS Block
     *
     * @return null|Mage_Cms_Model_Block
     */
    public function getCmsBlock()
    {
        $cms_block = $this->getData('cms_block');
        if(is_null($cms_block)){
            $identifier = $this->getItemData('cms_block_identifier', null);
            if(!$identifier){
                return null;
            }

            $cms_block = Mage::getModel('cms/block')->load($identifier, 'identifier');
            if(!$cms_block->getIsActive()){
                return null;
            }

            $this->setData('cms_block', $cms_block);
        }
        return $this->getData('cms_block');
    }

    /**
     * Get the HTML content of the block
     *
     * @return null|string
     */
    public function getContent()
    {
        $content = $this->getData('content');
        if(is_null($content)){
            $cms_block = $this->getCmsBlock();

            if(is_null($cms_block)){
                return null;
            }

            $helper = Mage::helper('cms');
            $processor = $helper->getBlockTemplateProcessor();
            $content = $processor->filter($cms_block->getContent());

            if(empty($content)){
                return null;
            }

            $this->setData('content', $content);
        }
        return $this->getData('content');
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $content = $this->getContent();
        if(!empty($content)){
            return parent::canDisplay();
        }
        return false;
    }
}