<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item_Megamenu_Youtube extends Magemonks_Menumanager_Block_Item {

    /**
     * Get the embed code
     *
     * @return null|string
     */
    public function getEmbedCode()
    {
        $embedCode =  $this->getItemData('embed_code');

        if(empty($embedCode)) return null;

        $embedCode = str_replace('<iframe', '<iframe id="'.$this->getEmbedId().'"', $embedCode);

        if( Mage::app()->getStore()->isCurrentlySecure()){
            $embedCode = str_replace('http://', 'https://', $embedCode );
        }
        else{
            $embedCode = str_replace('https://', 'http://', $embedCode );
        }

        return $embedCode;

    }

    /**
     * Get the embed id
     *
     * @return string
     */
    public function getEmbedId()
    {
        return 'menumanager-item-youtube-id-'.$this->getItemData('id');
    }


    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        $embedCode =  $this->getEmbedCode();

        if(!empty($embedCode)){
            return parent::canDisplay();
        }
        return false;
    }
}