<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Block_Item extends Mage_Core_Block_Template {
    protected $_childHtml;

    /**
     * Should the exact url be matched to determen if the item is active
     *
     * @var bool
     */
    protected $_exactUrlMatch = true;

    /**
     * Init the block
     *
     */
    public function init()
    {
        // Initialize the child blocks. Otherwise block-parents are not set on time.
        $this->getChildBlocks();

        $isLinkActive = $this->_isLinkActive();
        if($isLinkActive && $this->getParent()) {
            //only in case of active link, otherwise non-active links may overwrite the active state
            $this->getParent()->setIsLinkActiveParent(true);
        }
        $this->setData('is_link_active', $isLinkActive);
    }

    /**
     * Retrieves the child blocks for the current block
     *
     * @return Magemonks_Menumanager_Model_Item[]
     */
    public function getChildBlocks()
    {
        $childBlocks = $this->getData('child_blocks');
        if(is_null($childBlocks)){
            $blocks = array();

            //Dont get blocks when the (menu) maxChildDepth is reached
            $maxChildDepth = $this->getMenuData('max_child_depth');
            if(!empty($maxChildDepth) && is_numeric($maxChildDepth)){
                if($this->getData('level')+1 >= $maxChildDepth){
                    $this->setData('child_blocks', $blocks);
                    return $blocks;
                }
            }

            $children = $this->getItemChildren();
            $counter = 1;
            foreach($children as $child){
                if($child->canDisplay()){
                    $childBlocks = $child->getBlocks($this);
                    foreach($childBlocks as $childBlock){
                        if($childBlock->canDisplay()){
                            $childBlock->setData('level', $this->getData('level') + 1);
                            $childBlock->setData('counter', $counter);
                            $childBlock->setData('first', $counter == 1);
                            $childBlock->init();
                            $blocks[] = $childBlock;
                            $counter++;
                        }
                    }
                }
            }
            $lastBlock = end($blocks);
            if($lastBlock){
                $lastBlock->setData('last', true);
            }
            $this->setData('child_blocks', $blocks);
        }
        return $this->getData('child_blocks');
    }

    public function setIsLinkActiveParent($isLinkActiveParent)
    {
        $this->setData('is_link_active_parent', $isLinkActiveParent);
        if($this->getParent()) {
            $this->getParent()->setIsLinkActiveParent($isLinkActiveParent);
        }
    }

    /**
     * Set the menu item
     *
     * @param Magemonks_Menumanager_Model_Item $item
     */
    public function setItem(Magemonks_Menumanager_Model_Item $item)
    {
        $this->setData('item', $item);
    }

    /**
     * Retrieves the menu item
     *
     * @return null|Magemonks_Menumanager_Model_Item
     */
    public function getItem()
    {
        return $this->getData('item');
    }

    /**
     * Retrieves the menu
     *
     * @return null|Magemonks_Menumanager_Model_Menu
     */
    public function getMenu()
    {
        $item = $this->getItem();
        if($item){
            return $item->getMenu();
        }
        return null;
    }

    /**
     * Retrieve a property of the menu item
     *
     * @param null|string $key
     * @param null|string $index
     * @return mixed
     */
    public function getItemData($key = null, $index = null)
    {
        $item = $this->getData('item');
        if($item){
            return $this->getItem()->get($key, $index);
        }
        return null;
    }

    /**
     * Retrieve a property of the menu
     *
     * @param null|string $key
     * @param null|string $index
     * @return null
     */
    public function getMenuData($key = null, $index = null)
    {
        $menu = $this->getMenu();
        if($menu){
            return $menu->getData($key, $index);
        }
        return null;
    }

    /**
     *  Get the counter class
     * 
     * @return string
     * 
     */
    public function getCounterClass()
    {
        $counterClass = '';
        $block = $this;
        do{
            $counter = $block->getCounter();
            if(!is_null($counter) ){
                $counterClass = '-'.$block->getData('counter').$counterClass;
            }
        } while ($block = $block->getParent());
        return 'nav'.$counterClass;

    }

    /**
     * Get the item wrapper opening tag
     *
     * @return string
     */
    public function getItemWrapperOpen()
    {
        $classes     =  'menumanager-item '.
                        'menumanager-item-type-'.$this->getItemData('type').' '.
                        ($this->getItemTagClass() ? $this->getItemTagClass().' ' : '').
                        ($this->getChildAlignment() ? 'menumanager-item-child-alignment-'.$this->getChildAlignment().' ' : '').
                        'level'.(string) $this->getData('level').' '.
                        ($this->getItemData('width') ? 'menumanager-item-has-column-width menumanager-item-column-width-'.$this->getItemData('width').' ' : '').
                        ($this->getData('level') == 0 ? 'level-top ' : '').
                        ($this->getItemData('display_mode') ? 'menumanager-item-mode-'.$this->getItemData('display_mode').' ' : '').
                        ($this->getData('first') == true ? 'first ' : '').
                        ($this->getData('last') == true ? 'last ' : '').
                        ($this->getData('is_link_active') == true ? $this->getMenuData('item_active_class') . ' ' : '').
                        ($this->getData('is_link_active_parent') == true ? $this->getMenuData('item_active_parent_class') . ' ' : '').
                        ($this->getItemChildHtml() != "" ? 'parent ' : '').
                        $this->getCounterClass().' ';

        //extra class for megamenu-menu items
        if($this->getParent()->getItem()->connection == 'megamenu-menu' && $column = $this->getItem()->findParent('megamenu_column')){
            $columnWidth = $column->get('width');
            $counter = $this->getData('counter');

            if($counter % $columnWidth == 1){
                $classes .= 'menumanager-item-first-in-column ';
            }

            if($counter % $columnWidth == 0){
                $classes .= 'menumanager-item-last-in-column ';
            }

            if($counter <= $columnWidth){
                $classes .= 'menumanager-item-in-first-column-row ';
            }
        }

        //extra classes for megamenu (block) items
        if(in_array('width', $this->getItem()->configFields) && $column = $this->getItem()->findParent('megamenu_column')){
            $columnWidth = $column->get('width');

            $rows = array(array());
            $siblings = $this->getParent()->getChildBlocks();
            $itemFound = false;
            foreach($siblings as $sibling){
                if(!in_array('width', $sibling->getItem()->configFields)){
                    if($itemFound) break;
                    $rows = array(array());
                    continue;
                }
                $siblingWidth = $sibling->getItem()->get('width');
                $lastrowIndex = count($rows)-1;
                if(count($rows[$lastrowIndex]) + $siblingWidth > $columnWidth){
                    $lastrowIndex++;
                    $rows[$lastrowIndex] = array();
                }

                $itemsInRow = count($rows[$lastrowIndex]);
                for($i = $itemsInRow+1; $i<$itemsInRow+1+$siblingWidth; $i++){
                    $rows[$lastrowIndex][$i] = $sibling;
                }
                if($sibling == $this) $itemFound = true;
            }

            foreach($rows as $rowNum => $row){
                foreach($row as $itemNum => $item){
                    if($item == $this){
                        if($rowNum === 0){
                            $classes .= 'menumanager-item-in-first-column-row ';
                        }
                        if($itemNum === 1){
                            $classes .= 'menumanager-item-first-in-column ';
                        }
                        if($itemNum === count($row)){
                            $classes .= 'menumanager-item-last-in-column ';
                        }

                    }
                }
            }
        }

        $class       = sprintf(' class="%s"', trim($classes));

        $id          = $this->getItemData('item_tag_id') ? sprintf(' id="%s"', $this->getItemData('item_tag_id')) : '';

        return sprintf('<%1$s%2$s%3$s>', $this->getItemTagName(), $id, $class);
    }

    /**
     * Get the item wrapper closing tag
     *
     * @return string
     */
    public function getItemWrapperClose()
    {
        return sprintf('</%1$s>', $this->getItemTagName());
    }

    /**
     * Get the item tag (based on menu configuration)
     *
     * @return null|string
     */
    public function getItemTagName()
    {
        if($this->getItem()->connection == 'menu'){
            return $this->getMenuData('item_tag');
        }
        elseif($this->getItem()->connection == 'megamenu' || $this->getItem()->connection == 'megamenu-menu'){
            return $this->getMenuData('megamenu_item_tag');
        }
        elseif($this->getItem()->connection == 'menu-megamenu'){
            return $this->getMenuData('megamenu_tag');
        }
        return null;
    }

    /**
     * Get the item tag class
     *
     * @return null|string
     */
    public function getItemTagClass()
    {
        $class = ($this->getItemData('item_tag_class') != null ? $this->getItemData('item_tag_class').' ' : '');

        if($this->getItem()->connection == 'menu'){
            return $class.$this->getMenuData('item_tag_class');
        }
        elseif($this->getItem()->connection == 'megamenu' || $this->getItem()->connection == 'megamenu-menu'){
            return $class.$this->getMenuData('megamenu_item_tag_class');
        }
        elseif($this->getItem()->connection == 'menu-megamenu'){
            return $class.$this->getMenuData('megamenu_tag_class');
        }
        return $class;
    }

    /**
     * Get the image tag
     *
     * @param bool $use_max_prefix
     * @return string
     */
    public function getImageTag($use_max_prefix = false)
    {
        $image_url = $this->getItemData('image_url');
        if(!empty($image_url)){
            $alt = $this->getItemData('image_alt');

            $max = $use_max_prefix ? 'max-' : '';

            $width = $this->getItemData('image_width');
            $width = !empty($width) ? sprintf('%1$swidth:%2$spx;', $max, $width) : '';

            $height = $this->getItemData('image_height');
            $height = !empty($height) ? sprintf('%1$sheight:%2$spx;', $max, $height) : '';

            $style = !empty($width) || !empty($height) ?  sprintf(' style="%s"', $width.$height) : '';

            return sprintf('<img src="%1$s" alt="%2$s"%3$s />', $image_url, $alt, $style);
        }
    }

    /**
     * Get the child connection type
     *
     * @return null|string
     */
    public function getChildAlignment()
    {
        $children = $this->getChildBlocks();
        foreach($children as $child){
            if($child->getItem()->connection == 'menu-megamenu'){
                return $child->getItem()->get('alignment');
            }
        }
        return null;
    }

    /**
     * Get the submenu wrapper opening tag
     *
     * @return string
     */
    public function getSubMenuWrapperOpen()
    {
        if(!$this->isWrapChildrenWithSub()) return '';

        $classes     =  'menumanager-submenu '.
                        'menumanager-submenu_'.$this->getItemData('type').' '.
                        ($this->getMenuData('submenu_tag_class') != null ? $this->getMenuData('submenu_tag_class').' ' : '').
                        ($this->getItemData('submenu_tag_class') != null ? $this->getItemData('submenu_tag_class').' ' : '').
                        'level'.(string) $this->getData('level').' ';
        $class       = sprintf(' class="%s"', $classes);
        $id          = $this->getItemData('submenu_tag_id') ? sprintf(' id="%s"', $this->getItemData('submenu_tag_id')) : '';
        $tag         = $this->getMenuData('submenu_tag');
        return sprintf('<%1$s%2$s%3$s>', $tag, $id, $class);

    }

    /**
     * Get the submenu wrapper closing tag
     *
     * @return string
     */
    public function getSubMenuWrapperClose()
    {
        if(!$this->isWrapChildrenWithSub()) return '';

        $tag         = $this->getMenuData('submenu_tag') ? $this->getMenuData('submenu_tag') : 'ul';
        return sprintf('</%1$s>', $tag);
    }

    /**
     * Get the anchor opening tag
     *
     * @return string
     */
    public function getAnchorOpen()
    {
        $classes        = 'menumanager-anchor '.
                           ($this->getItemData('anchor_tag_class') ? $this->getItemData('anchor_tag_class').' ' : '').
                           ($this->getAnchorLink() != null ? '' : 'no-link ').
                            'level'.(string) $this->getData('level').' '.
                            $this->getCounterClass().' ';
        $class          = sprintf(' class="%s"', trim($classes));
        $id             = $this->getItemData('anchor_tag_id') ? sprintf(' id="%s"', $this->getItemData('anchor_tag_id')) : '';
        $rel            = $this->getItemData('rel') ? sprintf(' rel="%s"', $this->getItemData('rel')) : '';
        $target         = $this->getItemData('target') ? sprintf(' target="%s"', $this->getItemData('target')) : '';
        $href           = $this->getAnchorLink() != null ? sprintf(' href="%s"', $this->getAnchorLink()) : '';
        $style          = $this->getItemData('title_color') ? sprintf(" style='color: %s !important'", $this->getItemData('title_color')) : "";

        return sprintf('<a%1$s%2$s%3$s%4$s%5$s%6$s>', $class, $id, $rel, $target, $href, $style);
    }

    /**
     * Get the anchor closing tag
     *
     * @return string
     */
    public function getAnchorClose()
    {
        return '</a>';
    }

    /**
     * Get the anhor link
     *
     * @return null
     */
    public function getAnchorLink()
    {
        return null;
    }

    /**
     * Get the display mode
     *
     * @return null|string
     */
    public function getDisplayMode()
    {
        return $this->getItemData('display_mode');
    }
   
    /**
     * Get the title
     *
     * @return string
     */
    public function getTitle()
    {
        $title = $this->getItemData('title', null);
        if(empty($title)){
            return '';
        }
        return Mage::helper('menumanager')->__($title);
    }

    /**
     * Get the subtitle
     *
     * @return string
     */
    public function getSubTitle()
    {
        $title = $this->getItemData('sub_title', null);
        if(empty($title)){
            return '';
        }
        return Mage::helper('menumanager')->__($title);
    }

    /**
     * Get the counter value
     *
     * @return null|int
     */
    public function getCounter()
    {
        return $this->getData('counter');
    }

    /**
     * Get the increment of the counter
     *
     * @param int $counter
     * @return int
     */
    public function incrementCounter($counter)
    {
        return $counter+1;
    }

    /**
     * Set the child blocks for the current block
     *
     * @param mixed $blocks
     */
    public function setChildBlocks($blocks)
    {
        $this->_childBlocks = $blocks;
    }

    /**
     * Set the parent block
     *
     * @param $parent Magemonks_Menumanager_Block_Item
     */
    public function setParent($parent)
    {
        $this->setData('parent', $parent);
    }

    /**
     * Get the parent block
     *
     * @return null|Magemonks_Menumanager_Block_Item
     */
    public function getParent()
    {
        return $this->getData('parent');
    }

    /**
     * Get the children of the current model (submenu)
     *
     * @return Magemonks_Menumanager_Model_Item[]
     */
    public function getItemChildren()
    {

        return $this->getItem()->getChildren();
    }

    /**
     * Get the HTML of the children
     *
     * @return null|string
     */
    public function getItemChildHtml()
    {
        $childHtml = $this->getData('child_html');
        if(is_null($childHtml)){
            $html = array();
            foreach(($this->getChildBlocks()) as $block){
                $html[] = $block->toHtml();
            }
            $this->setData('child_html', implode('', $html));
        }
        return $this->getData('child_html');
    }

    /**
     * Should the items children be wrapped in a submenu
     *
     * @return bool
     */
    public function isWrapChildrenWithSub()
    {
        $children = $this->getChildBlocks();
        foreach($children as $child){
            if($child->getItem()->connection == 'megamenu' || $child->getItem()->connection == 'menu-megamenu') return false;
        }

        return true;
    }

    /**
     * Can the block be displayed
     *
     * @return bool
     */
    public function canDisplay()
    {
        if($this->getItemData('is_active') != true){
            return false;
        }

        $customers_group_ids = (array) $this->getItemData('customers_group_ids');
        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        if(count($customers_group_ids) > 0){
            if(!in_array($groupId, $customers_group_ids)){
                return false;
            }
        }

        if($this->getItemData('active_from')){
            $now = Mage::getSingleton('core/date')->timestamp();
            $from =  Mage::getSingleton('core/date')->timestamp($this->getItemData('active_from'));
            if($now < $from) return false;
        }

        if($this->getItemData('active_to')){
            $now = Mage::getSingleton('core/date')->timestamp();
            $to =  Mage::getSingleton('core/date')->timestamp($this->getItemData('active_to'));
            if($now > $to) return false;
        }

        return true;
    }

    /**
     * Is the current menu item active
     *
     * @return bool
     */
    protected function _isLinkActive()
    {
        $anchorLink = $this->getAnchorLink();
        if($this->getAnchorLink() === null) {
            return false;
        } else {
            $anchorLink = Mage::getModel('core/url')->sessionUrlVar($anchorLink);
            $currentUrl =  Mage::getModel('core/url')->sessionUrlVar(Mage::helper('core/url')->getCurrentUrl());
        }

        if($anchorLink === $currentUrl || ($this->getItemData('partial_url_match') && strpos($currentUrl, $anchorLink) >= 0)){
            return true;
        }
        return false;
    }
}