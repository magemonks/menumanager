<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Menumanager
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_Menumanager_Helper_Loader extends Mage_Core_Helper_Abstract{

    /**
     * Retreives the item types from the config and returns the model path in an array
     * @return array
     */
    public function getItemTypes()
    {
        $elements =  Mage::getConfig()->getFieldset('menumanager_itemtypes');
        $types = array();

        if(is_null($types)) return $types;

        if(!Mage::helper('magemonks/license')->isSerialValid(Mage::helper('menumanager')->getSerial(), Mage::helper('menumanager')->getProductCode())){
            return array();
        }

        foreach($elements as $type => $element){
            if(isset($element->modelpath) && !isset($element->disabled)){
                $canLoad = true;

                $moduleDependencies = (array) $element->depends->module;
                if(count($moduleDependencies)){
                    foreach($moduleDependencies as $moduleDependency){
                        $moduleEnabled = Mage::getConfig()->getModuleConfig($moduleDependency)->is('active', 'true');
                        if(!$moduleEnabled) $canLoad = false;
                    }
                }

                if($canLoad) $types[] = (string)$element->modelpath;
            }
        }

        return $types;
    }

    /**
     * Get the item type options
     * @return array
     */
    public function getItemTypesOptions()
    {
        $types = $this->getItemTypes();
        $options = array();
        foreach($types as $type){
            $options[] = array('value' => $type, 'label' => Mage::getModel($type)->getCreationLabel());
        }
        return $options;
    }
}